package com.magnate.software.gof.observer.custom;

import java.util.HashSet;
import java.util.Set;

/**
 * 主题角色实现类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 14:10
 */
public class CustomSubject implements Subject<String> {

    private Set<Observer> observers = new HashSet<Observer>();

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(String msg) {
        System.out.println("主题消息：" + msg);
        for (Observer observer : observers) {
            observer.update(msg);
        }
    }
}
