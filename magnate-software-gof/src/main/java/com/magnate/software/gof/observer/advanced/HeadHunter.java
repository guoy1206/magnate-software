package com.magnate.software.gof.observer.advanced;

import com.magnate.software.gof.observer.custom.Observer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 15:19
 */
public class HeadHunter implements JobSubject {

    private Map<Integer, Set<JobObserver>> observerMap;

    public HeadHunter() {
        observerMap = new HashMap<Integer, Set<JobObserver>>();
    }


    @Override
    public void registerObserver(JobObserver observer) {
        Integer jobType = observer.getJobType();
        Set<JobObserver> observers = observerMap.get(jobType);
        if (observers == null) {
            observers = new HashSet<JobObserver>();
        }
        observers.add(observer);
        observerMap.put(jobType, observers);
    }

    @Override
    public void removeObserver(JobObserver observer) {
        Integer jobType = observer.getJobType();
        Set<JobObserver> observers = observerMap.get(jobType);
        if (observers == null) {
            return;
        }
        observers.remove(observer);
        observerMap.put(jobType, observers);
    }

    @Override
    public void notifyObservers(JobMsg msg) {
        Set<JobObserver> observers = observerMap.get(msg.getJobType());
        if (observers == null) {
            return;
        }
        for (Observer observer : observers) {
            observer.update(msg);
        }
    }
}
