package com.magnate.software.gof.observer.custom;

/**
 *  观察者实现类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 14:10
 */
public class CustomObserver implements Observer<String> {

    private String observerName;

    public CustomObserver(String name) {
        this.observerName = name;
    }

    @Override
    public void update(String msg) {
        System.out.println(observerName + "-->消息：" + msg);
    }

}
