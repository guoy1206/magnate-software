package com.magnate.software.gof.observer.custom;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 14:07
 */
public interface Observer<T> {

    void update(T msg);

}
