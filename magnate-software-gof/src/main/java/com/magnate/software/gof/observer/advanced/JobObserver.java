package com.magnate.software.gof.observer.advanced;

import com.magnate.software.gof.observer.custom.Observer;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 17:25
 */
public class JobObserver implements Observer<JobMsg> {

    private Integer jobType;

    private String name;

    public JobObserver(Integer jobType, String name) {
        this.jobType = jobType;
        this.name = name;
    }

    public Integer getJobType() {
        return jobType;
    }

    @Override
    public void update(JobMsg msg) {
        System.out.println(name + " --> 监听类型：" + JobType.get(jobType).getName() + "[消息类型："  + JobType.get(msg.getJobType()).getName()  + " --> 信息：" + msg.getContents() + "]");
    }
}
