package com.magnate.software.gof.observer.jdk;


import java.util.Observable;
import java.util.Observer;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 14:10
 */
public class JdkCustomObserver implements Observer {

    private String observerName;

    public JdkCustomObserver(String name) {
        this.observerName = name;
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println(observerName + "-->消息：" + arg);
        System.out.println(observerName + "-->消息（拉取）：" + ((JdkCustomSubject) o).getMsg());
    }
}
