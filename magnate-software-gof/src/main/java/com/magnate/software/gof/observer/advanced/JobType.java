package com.magnate.software.gof.observer.advanced;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 17:27
 */
public enum JobType {

    JAVA(1, "java"),
    C(2, "c"),
    ANDROID(3, "android"),
    IOS(4, "ios");

    private Integer code;

    private String name;

    JobType(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static JobType get(Integer code) {
        for (JobType item : JobType.values()) {
            if (item.getCode().compareTo(code) == 0) {
                return item;
            }
        }
        return null;
    }
}
