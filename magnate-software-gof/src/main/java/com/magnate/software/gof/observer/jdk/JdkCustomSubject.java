package com.magnate.software.gof.observer.jdk;

import java.util.Observable;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 14:10
 */
public class JdkCustomSubject extends Observable {

    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setContents(String msg) {
        this.msg = msg;
        //注意在用Java中的Observer模式的时候，下面很重要、很重要、很重要
        this.setChanged();
        //然后主动通知，这里用的是推的方式
        this.notifyObservers(msg);
        //如果用拉的方式，这么调用
        //this.notifyObservers();
    }

}
