package com.magnate.software.gof.observer.advanced;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 17:34
 */
public interface JobSubject {

    void registerObserver(JobObserver observer);

    void removeObserver(JobObserver observer);

    void notifyObservers(JobMsg arg);
}
