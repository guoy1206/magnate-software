package com.magnate.software.gof.observer.custom;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 13:57
 */
public interface Subject<T> {

    void registerObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObservers(T arg);

}
