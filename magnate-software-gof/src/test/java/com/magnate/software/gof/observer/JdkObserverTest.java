package com.magnate.software.gof.observer;

import com.magnate.software.gof.observer.jdk.JdkCustomObserver;
import com.magnate.software.gof.observer.jdk.JdkCustomSubject;
import org.junit.Test;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 14:29
 */
public class JdkObserverTest {

    @Test
    public void observerTest() {
        //创建主题对象
        JdkCustomSubject subject = new JdkCustomSubject();
        //创建观察者对象
        JdkCustomObserver observer1 = new JdkCustomObserver("第1个观察者");
        subject.addObserver(observer1);
        JdkCustomObserver observer2 = new JdkCustomObserver("第2个观察者");
        subject.addObserver(observer2);
        JdkCustomObserver observer3 = new JdkCustomObserver("第3个观察者");
        subject.addObserver(observer3);
        //改变主题对象的状态
        subject.setContents("变动消息");
    }
}
