package com.magnate.software.gof.observer;

import com.magnate.software.gof.observer.advanced.HeadHunter;
import com.magnate.software.gof.observer.advanced.JobMsg;
import com.magnate.software.gof.observer.advanced.JobObserver;
import com.magnate.software.gof.observer.advanced.JobType;
import com.magnate.software.gof.observer.custom.CustomObserver;
import com.magnate.software.gof.observer.custom.CustomSubject;
import com.magnate.software.gof.observer.jdk.JdkCustomObserver;
import com.magnate.software.gof.observer.jdk.JdkCustomSubject;
import org.junit.Test;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/5/5 14:29
 */
public class CustomObserverTest {

    @Test
    public void observerTest() {
        //创建主题对象
        HeadHunter subject = new HeadHunter();
        //创建观察者对象
        JobObserver observer1 = new JobObserver(JobType.JAVA.getCode(),"JAVA观察者（1）");
        subject.registerObserver(observer1);
        JobObserver observer11 = new JobObserver(JobType.JAVA.getCode(),"JAVA观察者（2）");
        subject.registerObserver(observer11);
        JobObserver observer12 = new JobObserver(JobType.JAVA.getCode(),"JAVA观察者（3）");
        subject.registerObserver(observer12);

        JobObserver observer2 = new JobObserver(JobType.IOS.getCode(),"IOS观察者");
        subject.registerObserver(observer2);
        JobObserver observer3 = new JobObserver(JobType.ANDROID.getCode(),"ANDROID观察者");
        subject.registerObserver(observer3);
        //改变主题对象的状态
        JobMsg jobMsg = new JobMsg();
//        jobMsg.setJobType(JobType.JAVA.getCode());
//        jobMsg.setContents("这是一条JAVA招聘信息！");
//        subject.notifyObservers(jobMsg);

        jobMsg.setJobType(JobType.IOS.getCode());
        jobMsg.setContents("这是一条IOS招聘信息！");
        subject.notifyObservers(jobMsg);
//
//        jobMsg.setJobType(JobType.ANDROID.getCode());
//        jobMsg.setContents("这是一条ANDROID招聘信息！");
//        subject.notifyObservers(jobMsg);
    }
}
