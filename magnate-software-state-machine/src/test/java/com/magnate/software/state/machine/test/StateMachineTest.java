package com.magnate.software.state.machine.test;

import com.magnate.software.state.machine.StateMachineApplication;
import com.magnate.software.state.machine.enums.OrderEvents;
import com.magnate.software.state.machine.enums.OrderStates;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * 状态机测试类
 * <p>
 * Created by wangyang on 2017/6/2.
 */
@RunWith(SpringJUnit4ClassRunner.class)
//这是Spring Boot注解，为了进行集成测试，需要通过这个注解加载和配置Spring应用上下 注： 原为SpringApplicationConfiguration 在1.4以后过时
@SpringBootTest(classes = StateMachineApplication.class)
@WebAppConfiguration
public class StateMachineTest {

    @Autowired
    private StateMachine<OrderStates, OrderEvents> stateMachine;

    @Test
    public void orderStateTest() {
        stateMachine.start();
        stateMachine.sendEvent(OrderEvents.PAY);
        stateMachine.sendEvent(OrderEvents.DELIVERY);
        stateMachine.sendEvent(OrderEvents.RECEIVE);
    }

}
