package com.magnate.software.state.machine.config;

import com.magnate.software.state.machine.enums.OrderEvents;
import com.magnate.software.state.machine.enums.OrderStates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.transition.Transition;

import java.util.EnumSet;

/**
 * 状态机配置
 * <p>
 * Created by wangyang on 2017/6/2.
 */
@Configuration
@EnableStateMachine // 注解用来启用Spring StateMachine状态机功能
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<OrderStates, OrderEvents> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 初始化状态机状态值
     *
     * @param states
     * @throws Exception
     */
    @Override
    public void configure(StateMachineStateConfigurer<OrderStates, OrderEvents> states)
            throws Exception {
        states
                .withStates()
                .initial(OrderStates.UNPAID) // 初始状态
                .states(EnumSet.allOf(OrderStates.class)); // 全部状态
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<OrderStates, OrderEvents> transitions)
            throws Exception {
        transitions
                .withExternal()
                .source(OrderStates.UNPAID).target(OrderStates.WAITING_FOR_DELIVERY)
                .event(OrderEvents.PAY)
                .and()
                .withExternal()
                .source(OrderStates.WAITING_FOR_DELIVERY).target(OrderStates.WAITING_FOR_RECEIVE)
                .event(OrderEvents.DELIVERY)
                .and()
                .withExternal()
                .source(OrderStates.WAITING_FOR_RECEIVE).target(OrderStates.DONE)
                .event(OrderEvents.RECEIVE);
    }

    /*
    @Override
    public void configure(StateMachineConfigurationConfigurer<OrderStates, OrderEvents> config)
            throws Exception {
        config
                .withConfiguration()
                .listener(listener());
    }

    @Bean
    public StateMachineListener<OrderStates, OrderEvents> listener() {
        return new StateMachineListenerAdapter<OrderStates, OrderEvents>() {

            @Override
            public void transition(Transition<OrderStates, OrderEvents> transition) {
                switch (transition.getTarget().getId()) {
                    case UNPAID:
                        logger.info("订单创建，待支付");
                        return;
                    case WAITING_FOR_DELIVERY:
                        if (transition.getSource().getId() == OrderStates.UNPAID) {
                            logger.info("用户完成支付，待发货");
                            return;
                        }
                    case WAITING_FOR_RECEIVE:
                        if (transition.getSource().getId() == OrderStates.WAITING_FOR_DELIVERY) {
                            logger.info("发货完成，用户待收货");
                            return;
                        }
                    case DONE:
                        if (transition.getSource().getId() == OrderStates.WAITING_FOR_RECEIVE) {
                            logger.info("用户已收货，订单完成");
                            return;
                        }
                }
            }
        };
    }
    */

}
