package com.magnate.software.state.machine.enums;

/**
 * 订单状态
 * <p>
 * Created by wangyang on 2017/6/2.
 */
public enum OrderStates {
    UNPAID,                 // 待支付
    WAITING_FOR_DELIVERY,    // 待发货
    WAITING_FOR_RECEIVE,    // 待收货
    DONE                    // 结束
}
