package com.magnate.software.state.machine.enums;

/**
 * 订单动作
 * <p>
 * Created by wangyang on 2017/6/2.
 */
public enum OrderEvents {

    PAY,        // 支付
    DELIVERY,   // 发货
    RECEIVE     // 收货

}
