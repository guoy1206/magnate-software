package com.magnate.software.state.machine.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;

/**
 * 订单状态配置
 * <p>
 * Created by wangyang on 2017/6/2.
 */
@WithStateMachine
public class OrderEventConfig {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @OnTransition(target = "UNPAID")
    public void create() {
        logger.info("订单创建，待支付");
    }

    @OnTransition(source = "UNPAID", target = "WAITING_FOR_DELIVERY")
    public void pay() {
        logger.info("用户完成支付，待发货");
    }

    @OnTransition(source = "WAITING_FOR_DELIVERY", target = "WAITING_FOR_RECEIVE")
    public void send() {
        logger.info("发货完成，用户待收货");
    }

    @OnTransition(source = "WAITING_FOR_RECEIVE", target = "DONE")
    public void receive() {
        logger.info("用户已收货，订单完成");
    }

}
