/*
 * Project: greenline-hrs-out-service
 * 
 * File Created at 2015-09-10
 
 * Copyright 2012 Greenline.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Greenline Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Greenline.com.
 */
package com.magnate.software.spring.fox.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * TODO
 *
 * @author guolinlin
 * @version V1.0
 * @since 2015-09-10 16:09
 */
@Api(value = "SwaggerConTest", description = "SwaggerConTest", position = 6)
@Controller
@RequestMapping("/SwaggerConTest")

public class SwaggerConTest {

    @ApiOperation(value = "创建用户")
    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    public void add(@RequestBody Department contact) {
    }

    @ApiOperation(value = "更新用户 1.0")
    @RequestMapping(value = "/contact", method = RequestMethod.PUT, headers = "apiVersion=1.0")
    @ResponseBody
    public void update(@RequestBody Department contact) {
    }

    @ApiOperation(value = "更新用户 1.1")
    @RequestMapping(value = "/contact", method = RequestMethod.PUT, headers = "apiVersion=1.1")
    @ResponseBody
    public void updateNew(@RequestBody Department contact) {
    }
}
