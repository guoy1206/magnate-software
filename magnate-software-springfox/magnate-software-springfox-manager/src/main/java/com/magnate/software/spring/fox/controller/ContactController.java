/*
 * Project: greenline-hrs-out-service
 * 
 * File Created at 2015-09-10
 
 * Copyright 2012 Greenline.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Greenline Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Greenline.com.
 */
package com.magnate.software.spring.fox.controller;

import io.swagger.annotations.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;

@Api(value = "contacts-api", description = "有关于用户的CURD操作", position = 5, protocols = "https")
@Controller
@RequestMapping("/contacts")
public class ContactController {
    /*@Autowired
    ContactService contactService;*/
    @ResponseBody
    @RequestMapping(value = "/1.0/contact/get.do/{id}", method = RequestMethod.GET)
    public Department get(@PathVariable Long id) {
        return null;
    }

    @ApiOperation(value = "创建用户", notes = "返回用户实体对象", response = Department.class, position = 2)
    @ApiImplicitParams({@ApiImplicitParam(name = "apiVersion", value = "API版本", dataType = "String", paramType = "header", defaultValue = "1.0")})
    @RequestMapping(value = "/1.0/contact/add.do", method = RequestMethod.POST)
    public void add(@RequestBody Department contact, HttpServletResponse response) {
        //contactService.create(contact);
        String location = ServletUriComponentsBuilder.fromCurrentRequest().pathSegment("{id}").buildAndExpand("")
                .toUriString();

        response.setHeader("Location", location);
    }

    @RequestMapping(value = "/1.0/contact/update.do/{id}", method = RequestMethod.POST)
    // @ApiImplicitParams({@ApiImplicitParam(value = "部门ID",)})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "更新成功", response = Department.class),
            @ApiResponse(code = 404, message = "找不到页面"),
            @ApiResponse(code = 500, message = "内部报错")})
    public void update(@PathVariable Integer id,
                       @RequestBody Department contact) {
        // contact.setId(id);;
        // contactService.update(contact);
    }

    @RequestMapping(value = "/1.0/contact/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@ApiParam(name = "userId", required = true, value = "用户ID") @RequestHeader(value = "userId", defaultValue = "2090543") Long userId,
                       @ApiParam(name = "id", required = true, value = "编号") @PathVariable(value = "id") Integer id) throws Exception {
        System.out.println(userId + " " + id);
    }
}
