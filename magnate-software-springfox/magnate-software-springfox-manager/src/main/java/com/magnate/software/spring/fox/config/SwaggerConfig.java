package com.magnate.software.spring.fox.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/11/14 20:06
 */
@Configuration
@EnableSwagger2
@EnableWebMvc
@ComponentScan("com.magnate.software.spring.fox.controller")
public class SwaggerConfig {

    public static final String SWAGGER_SCAN_BASE_PACKAGE = "com.magnate.software.spring.fox.controller";
    public static final String VERSION = "1.0.0";

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Swagger API")
                .description("This is to show api description")
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .termsOfServiceUrl("")
                .version(VERSION)
                .contact(new Contact("", "", "miaorf@outlook.com"))
                .build();
    }

    ApiInfo testApiInfo() {
        return new ApiInfoBuilder()
                .title("Test API")
                .description("This is to show api description")
                .termsOfServiceUrl("")
                .version(VERSION)
                .contact(new Contact("miaorf", "", "miaorf@outlook.com"))
                .build();
    }

        /*
    @Bean
    public Docket customImplementation() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("联系方式")
                //.genericModelSubstitutes(Department.class)
                .useDefaultResponseMessages(false)
                .forCodeGeneration(true)
                .select()
                .paths(PathSelectors.regex("/contacts/.*"))
                .build()
                .apiInfo(apiInfo());
    }
    */

    @Bean
    public Docket testApi() {
        return new Docket(DocumentationType.SPRING_WEB)
                .groupName("test")
                .genericModelSubstitutes(DeferredResult.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .useDefaultResponseMessages(false)
                .forCodeGeneration(true)
                .select()
                .paths(PathSelectors.regex("/SwaggerConTest/.*"))
                //.apis(RequestHandlerSelectors.basePackage("com.magnate.software.spring.fox.controller.SwaggerConTest"))
                .build()
                .apiInfo(testApiInfo());
    }

}
