package com.magnate.software.shiro.service.impl;

import com.magnate.software.shiro.crypto.PasswordEncryptService;
import com.magnate.software.shiro.dao.AccountAuthorizationDao;
import com.magnate.software.shiro.dao.AccountDao;
import com.magnate.software.shiro.domain.po.Account;
import com.magnate.software.shiro.domain.po.AccountAuthorization;
import com.magnate.software.shiro.service.AccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/12 10:26
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Resource
    private AccountDao accountDao;

    @Resource
    private AccountAuthorizationDao accountAuthorizationDao;

    @Resource
    private PasswordEncryptService passwordEncryptService;

    @Override
    public Account create(Account account) {
        //加密密码
        passwordEncryptService.encryptPassword(account);
        accountDao.create(account);
        return account;
    }

    @Override
    public void changePassword(String account, String newPassword) {

    }

    @Override
    public List<Account> query(Account account) {
        return accountDao.retrieve(account);
    }

    @Override
    public Account query(String account) {
        return accountDao.retrieve(account);
    }

    @Override
    public Set<String> queryRoles(String account) {
        Set<String> set = new HashSet<String>();
        set.addAll(accountDao.queryRoles(account));
        return set;
    }

    @Override
    public Set<String> queryPermissions(String account) {
        Set<String> set = new HashSet<String>();
        set.addAll(accountDao.queryPermissionsByRoles(account));
        return set;
    }

    @Override
    public void authorization(AccountAuthorization authorization) {
        accountAuthorizationDao.create(authorization);
    }
}
