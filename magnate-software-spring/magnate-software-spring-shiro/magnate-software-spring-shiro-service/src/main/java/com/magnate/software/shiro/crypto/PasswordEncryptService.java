package com.magnate.software.shiro.crypto;

import com.magnate.software.shiro.domain.po.Account;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/12 10:45
 */
public interface PasswordEncryptService {

    /**
     * 设置账户密码
     *
     * @param account 账户
     */
    void encryptPassword(Account account);
}
