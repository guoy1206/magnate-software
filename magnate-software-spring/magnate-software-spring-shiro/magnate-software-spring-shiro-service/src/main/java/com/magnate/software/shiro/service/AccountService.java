package com.magnate.software.shiro.service;

import com.magnate.software.shiro.domain.po.Account;
import com.magnate.software.shiro.domain.po.AccountAuthorization;
import com.magnate.software.shiro.domain.po.Function;

import java.util.List;
import java.util.Set;

/**
 * 账户服务接口
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/11 16:19
 */
public interface AccountService {

    /**
     * 创建用户
     *
     * @param account
     */
    Account create(Account account);

    /**
     * 修改密码
     *
     * @param account     账户名
     * @param newPassword 新密码
     */
    void changePassword(String account, String newPassword);

    /**
     * 查询账户列表
     *
     * @return
     */
    List<Account> query(Account account);

    /**
     * 根据用户名查找用户
     *
     * @param account 账户名
     * @return
     */
    Account query(String account);

    /**
     * 根据用户名查找其角色
     *
     * @param account 账户名
     * @return
     */
    Set<String> queryRoles(String account);

    /**
     * 根据用户名查找其权限
     *
     * @param account      账户名
     * @return
     */
    Set<String> queryPermissions(String account);

    /**
     * 为用户授权
     *
     * @param authorization 授权信息
     */
    void authorization(AccountAuthorization authorization);

}
