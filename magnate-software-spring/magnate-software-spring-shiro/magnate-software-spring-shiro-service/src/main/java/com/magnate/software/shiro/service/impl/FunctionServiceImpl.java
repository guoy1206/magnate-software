package com.magnate.software.shiro.service.impl;

import com.magnate.software.shiro.dao.FunctionDao;
import com.magnate.software.shiro.domain.enums.CodeConstants;
import com.magnate.software.shiro.domain.po.Function;
import com.magnate.software.shiro.service.FunctionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能权限服务类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/14 10:23
 */
@Service("functionService")
public class FunctionServiceImpl implements FunctionService {

    @Resource
    private FunctionDao functionDao;

    @Override
    public Function query(String functionCode) {
        return functionDao.retrieve(functionCode);
    }

    @Override
    public List<Function> query(Function function) {
        return functionDao.retrieve(function);
    }

    @Override
    public int delete(String functionCode) {
        return functionDao.delete(functionCode);
    }

    @Override
    public int delete(Function function) {
        return functionDao.delete(function);
    }

    @Override
    public Function create(Function function) throws Exception {
        functionDao.create(function);
        return function;
    }

    @Override
    public int update(Function function) throws Exception {
        return functionDao.create(function);
    }

    @Override
    public List<Function> queryPermissions(String account, Integer functionType) {
        Map<String, Object> params = new HashMap<String, Object>(3);
        params.put("account", account);
        params.put("functionType", functionType);
        params.put("authorizationType", CodeConstants.AUTHORIZATION_TYPE_ROLE.getCode());
        return functionDao.queryPermissions(params);
    }
}
