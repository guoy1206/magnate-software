package com.magnate.software.shiro.service;

import com.magnate.software.shiro.domain.po.Role;
import com.magnate.software.shiro.domain.po.RoleFunction;

import java.util.List;

/**
 * 角色服务接口
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/13 14:59
 */
public interface RoleService {

    /**
     * 根据主键查询Role对象
     *
     * @param roleCode
     * @return Role对象
     */
    Role query(String roleCode);

    /**
     * 根据查询条件查询符合条件的Role对象
     *
     * @param role
     * @return 符合条件的Role对象List
     */
    List<Role> query(Role role);

    /**
     * 根据主键删除Role对象
     *
     * @param roleCode
     * @return 影响条件数
     */
    int delete(String roleCode);

    /**
     * 根据条件删除符合条件的Role对象
     *
     * @param role
     * @return 影响条件数
     */
    int delete(Role role);

    /**
     * 插入Role对象
     *
     * @param role
     * @return 影响条件数
     */
    Role create(Role role);

    /**
     * 更新Role对象
     *
     * @param role
     * @return 影响条件数
     */
    int update(Role role);

    /**
     * 为角色授权
     *
     * @param authorization     授权信息
     */
    void authorization(RoleFunction authorization);
}
