package com.magnate.software.shiro.service.impl;

import com.magnate.software.shiro.dao.RoleDao;
import com.magnate.software.shiro.dao.RoleFunctionDao;
import com.magnate.software.shiro.domain.po.Role;
import com.magnate.software.shiro.domain.po.RoleFunction;
import com.magnate.software.shiro.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 角色服务类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/14 10:10
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleDao roleDao;

    @Resource
    private RoleFunctionDao roleFunctionDao;

    @Override
    public Role query(String roleCode) {
        return roleDao.retrieve(roleCode);
    }

    @Override
    public List<Role> query(Role role) {
        return roleDao.retrieve(role);
    }

    @Override
    public int delete(String roleCode) {
        return roleDao.delete(roleCode);
    }

    @Override
    public int delete(Role role) {
        return roleDao.delete(role);
    }

    @Override
    public Role create(Role role) {
        roleDao.create(role);
        return role;
    }

    @Override
    public int update(Role role) {
        return roleDao.update(role);
    }

    @Override
    public void authorization(RoleFunction authorization) {
        roleFunctionDao.create(authorization);
    }
}
