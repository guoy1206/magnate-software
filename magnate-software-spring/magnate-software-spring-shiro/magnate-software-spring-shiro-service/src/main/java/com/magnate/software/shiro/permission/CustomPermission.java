package com.magnate.software.shiro.permission;

import org.apache.shiro.authz.Permission;

import java.io.Serializable;
import java.util.List;

/**
 * 自定义权限
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/11 16:39
 */
public class CustomPermission implements Permission, Serializable {

    private List<String> permissions;

    public CustomPermission(List<String> permissions) {
        this.permissions = permissions;
    }

    @Override
    public boolean implies(Permission p) {
        return false;
    }

}
