package com.magnate.software.shiro.credentials;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 凭证匹配器
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/5 11:14
 */
public class RetryLimitHashedCredentialsMatcher extends HashedCredentialsMatcher {

    private Integer retry = 3;

    private Cache<String, AtomicInteger> passwordRetryCache;

    public RetryLimitHashedCredentialsMatcher(CacheManager cacheManager) {
        passwordRetryCache = cacheManager.getCache("passwordRetryCache");
    }

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        String username = (String) token.getPrincipal();
        // 获取缓存中用户登录重试次数
        AtomicInteger retryCount = passwordRetryCache.get(username);
        //判断重试次数是否为空
        if (retryCount == null) {
            retryCount = new AtomicInteger(0);
            passwordRetryCache.put(username, retryCount);
        } else if (retryCount.incrementAndGet() > retry) { // 判断用户登录重试次数是否大于最重试次数
            throw new ExcessiveAttemptsException();
        }
        // 凭证匹配
        boolean matches = super.doCredentialsMatch(token, info);
        if (matches) {
            // 匹配通过，删除缓存中的重试次数
            passwordRetryCache.remove(username);
        }
        return matches;
    }

    public void setRetry(Integer retry) {
        this.retry = retry;
    }
}
