package com.magnate.software.shiro.dao;

import com.magnate.software.shiro.domain.po.Account;

import java.util.List;

/**
 * 帐户表数据操作接口
 *
 * @author: WangYang
 * @version: 1.0
 * @datetime: 2016-01-11 16:16:02
 */
public interface AccountDao {

    /**
     * 根据主键查询Account对象
     *
     * @param account
     * @return Account对象
     */
    Account retrieve(String account);

    /**
     * 根据查询条件查询符合条件的Account对象
     *
     * @param account
     * @return 符合条件的Account对象List
     */
    List<Account> retrieve(Account account);

    /**
     * 根据主键删除Account对象
     *
     * @param account
     * @return 影响条件数
     */
    int delete(String account);

    /**
     * 根据条件删除符合条件的Account对象
     *
     * @param account
     * @return 影响条件数
     */
    int delete(Account account);

    /**
     * 插入Account对象
     *
     * @param account
     * @return 影响条件数
     */
    int create(Account account);

    /**
     * 更新Account对象
     *
     * @param account
     * @return 影响条件数
     */
    int update(Account account);

    /**
     * 查询账户角色列表
     *
     * @param account 账户名
     * @return
     */
    List<String> queryRoles(String account);

    /**
     * 查询账户权限列表
     *
     * @param account      账户名
     * @return
     */
    List<String> queryPermissionsByRoles(String account);

}
