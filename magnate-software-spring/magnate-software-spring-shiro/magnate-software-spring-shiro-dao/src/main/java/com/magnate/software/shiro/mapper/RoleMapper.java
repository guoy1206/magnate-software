package com.magnate.software.shiro.mapper;

import com.magnate.software.shiro.domain.po.Role;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
* 角色表数据库操作接口
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
@Repository("roleMapper")
public interface RoleMapper {

    /**
    *
    * 根据主键查询Role对象
    *
    * @param roleCode
    * @return Role对象
    */
    Role selectByPrimaryKey(String roleCode);

    /**
    *
    * 根据查询条件查询符合条件的Role对象
    *
    * @param role
    * @return 符合条件的Role对象List
    */
    List<Role> select(Role role);

    /**
    *
    * 根据主键删除Role对象
    *
    * @param roleCode
    * @return 影响条件数
    */
    int deleteByPrimaryKey(String roleCode);

    /**
    *
    * 根据条件删除符合条件的Role对象
    *
    * @param role
    * @return 影响条件数
    */
    int delete(Role role);

    /**
    *
    * 插入Role对象
    *
    * @param role
    * @return 影响条件数
    */
    int insert(Role role);

    /**
    *
    * 更新Role对象
    *
    * @param role
    * @return 影响条件数
    */
    int update(Role role);

}
