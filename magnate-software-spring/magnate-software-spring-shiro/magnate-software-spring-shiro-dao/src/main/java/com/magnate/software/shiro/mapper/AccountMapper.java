package com.magnate.software.shiro.mapper;

import com.magnate.software.shiro.domain.po.Account;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 帐户表数据库操作接口
 *
 * @author: WangYang
 * @version: 1.0
 * @datetime: 2016-01-11 16:16:02
 */
@Repository("accountMapper")
public interface AccountMapper {

    /**
     * 根据主键查询Account对象
     *
     * @param account 账户名
     * @return Account对象
     */
    Account selectByPrimaryKey(String account);

    /**
     * 根据查询条件查询符合条件的Account对象
     *
     * @param account
     * @return 符合条件的Account对象List
     */
    List<Account> select(Account account);

    /**
     * 根据主键删除Account对象
     *
     * @param account
     * @return 影响条件数
     */
    int deleteByPrimaryKey(String account);

    /**
     * 根据条件删除符合条件的Account对象
     *
     * @param account
     * @return 影响条件数
     */
    int delete(Account account);

    /**
     * 插入Account对象
     *
     * @param account
     * @return 影响条件数
     */
    int insert(Account account);

    /**
     * 更新Account对象
     *
     * @param account
     * @return 影响条件数
     */
    int update(Account account);

    /**
     * 查询账户角色列表
     *
     * @param account 账户名
     * @return
     */
    List<String> selectRoles(@Param("account") String account);

    /**
     * 查询账户权限列表
     *
     * @param account      账户名
     * @return
     */
    List<String> selectPermissionsByRoles(@Param("account") String account);

}
