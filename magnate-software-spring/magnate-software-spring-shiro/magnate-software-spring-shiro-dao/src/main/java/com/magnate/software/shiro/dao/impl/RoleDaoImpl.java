package com.magnate.software.shiro.dao.impl;

import com.magnate.software.shiro.dao.RoleDao;
import com.magnate.software.shiro.domain.enums.CodeConstants;
import com.magnate.software.shiro.mapper.RoleMapper;
import com.magnate.software.shiro.domain.po.Role;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
* 角色表数据操作实现
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
@Service("roleDao")
public class RoleDaoImpl implements RoleDao {

    @Resource
    private RoleMapper roleMapper;

    @Override
    public Role retrieve(String roleCode) {
        return roleMapper.selectByPrimaryKey(roleCode);
    }

    @Override
    public List<Role> retrieve(Role role) {
        return roleMapper.select(role);
    }

    @Override
    public int delete(String roleCode) {
        return roleMapper.deleteByPrimaryKey(roleCode);
    }

    @Override
    public int delete(Role role) {
        return roleMapper.delete(role);
    }

    @Override
    public int create(Role role) {
        role.setRoleStatus(CodeConstants.ROLE_STATUS_NORMAL.getCode());
        role.setCreateTime(new Date());
        return roleMapper.insert(role);
    }

    @Override
    public int update(Role role) {
        role.setUpdateTime(new Date());
        return roleMapper.update(role);
    }
}
