package com.magnate.software.shiro.dao.impl;

import com.magnate.software.shiro.dao.AccountDao;
import com.magnate.software.shiro.domain.enums.CodeConstants;
import com.magnate.software.shiro.domain.po.Account;
import com.magnate.software.shiro.mapper.AccountMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * 帐户表数据操作实现
 *
 * @author: WangYang
 * @version: 1.0
 * @datetime: 2016-01-11 16:16:02
 */
@Service("accountDao")
public class AccountDaoImpl implements AccountDao {

    @Resource
    private AccountMapper accountMapper;

    @Override
    public Account retrieve(String account) {
        return accountMapper.selectByPrimaryKey(account);
    }

    @Override
    public List<Account> retrieve(Account account) {
        return accountMapper.select(account);
    }

    @Override
    public int delete(String account) {
        return accountMapper.deleteByPrimaryKey(account);
    }

    @Override
    public int delete(Account account) {
        return accountMapper.delete(account);
    }

    @Override
    public int create(Account account) {
        // 账户状态（正常）
        account.setAccountStatus(CodeConstants.ACCOUNT_STATUS_NORMAL.getCode());
        // 建立时间
        account.setCreateTime(new Date());
        return accountMapper.insert(account);
    }

    @Override
    public int update(Account account) {
        // 更新时间
        account.setUpdateTime(new Date());
        return accountMapper.update(account);
    }

    @Override
    public List<String> queryRoles(String account) {
        return accountMapper.selectRoles(account);
    }

    @Override
    public List<String> queryPermissionsByRoles(String account) {
        return accountMapper.selectPermissionsByRoles(account);
    }
}
