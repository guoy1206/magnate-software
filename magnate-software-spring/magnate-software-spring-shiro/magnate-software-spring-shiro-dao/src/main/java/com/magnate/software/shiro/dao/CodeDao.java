package com.magnate.software.shiro.dao;

import com.magnate.software.shiro.domain.po.Code;
import com.magnate.software.shiro.domain.po.CodeKey;
import java.util.List;

/**
* 字典表数据操作接口
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
public interface CodeDao {

    /**
    *
    * 根据主键查询Code对象
    *
    * @param key
    * @return Code对象
    */
    Code retrieve(CodeKey key);

    /**
    *
    * 根据查询条件查询符合条件的Code对象
    *
    * @param code
    * @return 符合条件的Code对象List
    */
    List<Code> retrieve(Code code);

    /**
    *
    * 根据主键删除Code对象
    *
    * @param key
    * @return 影响条件数
    */
    int delete(CodeKey key);

    /**
    *
    * 根据条件删除符合条件的Code对象
    *
    * @param code
    * @return 影响条件数
    */
    int delete(Code code);

    /**
    *
    * 插入Code对象
    *
    * @param code
    * @return 影响条件数
    */
    int create(Code code);

    /**
    *
    * 更新Code对象
    *
    * @param code
    * @return 影响条件数
    */
    int update(Code code);

}
