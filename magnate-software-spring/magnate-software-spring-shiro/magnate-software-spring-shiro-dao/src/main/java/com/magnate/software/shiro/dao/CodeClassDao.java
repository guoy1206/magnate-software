package com.magnate.software.shiro.dao;

import com.magnate.software.shiro.domain.po.CodeClass;
import java.util.List;

/**
* 字典分类表数据操作接口
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
public interface CodeClassDao {

    /**
    *
    * 根据主键查询CodeClass对象
    *
    * @param codeClassId
    * @return CodeClass对象
    */
    CodeClass retrieve(String codeClassId);

    /**
    *
    * 根据查询条件查询符合条件的CodeClass对象
    *
    * @param codeClass
    * @return 符合条件的CodeClass对象List
    */
    List<CodeClass> retrieve(CodeClass codeClass);

    /**
    *
    * 根据主键删除CodeClass对象
    *
    * @param codeClassId
    * @return 影响条件数
    */
    int delete(String codeClassId);

    /**
    *
    * 根据条件删除符合条件的CodeClass对象
    *
    * @param codeClass
    * @return 影响条件数
    */
    int delete(CodeClass codeClass);

    /**
    *
    * 插入CodeClass对象
    *
    * @param codeClass
    * @return 影响条件数
    */
    int create(CodeClass codeClass);

    /**
    *
    * 更新CodeClass对象
    *
    * @param codeClass
    * @return 影响条件数
    */
    int update(CodeClass codeClass);

}
