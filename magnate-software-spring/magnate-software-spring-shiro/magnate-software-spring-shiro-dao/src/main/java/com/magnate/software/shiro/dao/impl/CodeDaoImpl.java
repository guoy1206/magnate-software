package com.magnate.software.shiro.dao.impl;

import com.magnate.software.shiro.dao.CodeDao;
import com.magnate.software.shiro.domain.enums.CodeConstants;
import com.magnate.software.shiro.domain.po.Code;
import com.magnate.software.shiro.domain.po.CodeKey;
import com.magnate.software.shiro.mapper.CodeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * 字典表数据操作实现
 *
 * @author: WangYang
 * @version: 1.0
 * @datetime: 2016-01-14 17:21:45
 */
@Service("codeDao")
public class CodeDaoImpl implements CodeDao {

    @Resource
    private CodeMapper codeMapper;

    @Override
    public Code retrieve(CodeKey key) {
        return codeMapper.selectByPrimaryKey(key);
    }

    @Override
    public List<Code> retrieve(Code code) {
        return codeMapper.select(code);
    }

    @Override
    public int delete(CodeKey key) {
        return codeMapper.deleteByPrimaryKey(key);
    }

    @Override
    public int delete(Code code) {
        return codeMapper.delete(code);
    }

    @Override
    public int create(Code code) {
        code.setCodeStatus(CodeConstants.CODE_STATUS_NORMAL.getCode());
        code.setCreateTime(new Date());
        return codeMapper.insert(code);
    }

    @Override
    public int update(Code code) {
        code.setUpdateTime(new Date());
        return codeMapper.update(code);
    }
}
