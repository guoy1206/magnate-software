package com.magnate.software.shiro.mapper;

import com.magnate.software.shiro.domain.po.Function;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 功能权限表数据库操作接口
 *
 * @author: WangYang
 * @version: 1.0
 * @datetime: 2016-01-14 17:21:45
 */
@Repository("functionMapper")
public interface FunctionMapper {

    /**
     * 根据主键查询Function对象
     *
     * @param functionCode
     * @return Function对象
     */
    Function selectByPrimaryKey(String functionCode);

    /**
     * 根据查询条件查询符合条件的Function对象
     *
     * @param function
     * @return 符合条件的Function对象List
     */
    List<Function> select(Function function);

    /**
     * 根据主键删除Function对象
     *
     * @param functionCode
     * @return 影响条件数
     */
    int deleteByPrimaryKey(String functionCode);

    /**
     * 根据条件删除符合条件的Function对象
     *
     * @param function
     * @return 影响条件数
     */
    int delete(Function function);

    /**
     * 插入Function对象
     *
     * @param function
     * @return 影响条件数
     */
    int insert(Function function);

    /**
     * 更新Function对象
     *
     * @param function
     * @return 影响条件数
     */
    int update(Function function);

    /**
     * 根据用户名查找其权限
     *
     * @param params 查询参数
     * @return
     */
    List<Function> selectPermissions(Map<String, Object> params);

}
