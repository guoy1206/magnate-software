package com.magnate.software.shiro.dao;

import com.magnate.software.shiro.domain.po.RoleFunction;
import com.magnate.software.shiro.domain.po.RoleFunctionKey;
import java.util.List;

/**
* 角色权限表数据操作接口
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
public interface RoleFunctionDao {

    /**
    *
    * 根据主键查询RoleFunction对象
    *
    * @param key
    * @return RoleFunction对象
    */
    RoleFunction retrieve(RoleFunctionKey key);

    /**
    *
    * 根据查询条件查询符合条件的RoleFunction对象
    *
    * @param roleFunction
    * @return 符合条件的RoleFunction对象List
    */
    List<RoleFunction> retrieve(RoleFunction roleFunction);

    /**
    *
    * 根据主键删除RoleFunction对象
    *
    * @param key
    * @return 影响条件数
    */
    int delete(RoleFunctionKey key);

    /**
    *
    * 根据条件删除符合条件的RoleFunction对象
    *
    * @param roleFunction
    * @return 影响条件数
    */
    int delete(RoleFunction roleFunction);

    /**
    *
    * 插入RoleFunction对象
    *
    * @param roleFunction
    * @return 影响条件数
    */
    int create(RoleFunction roleFunction);

    /**
    *
    * 更新RoleFunction对象
    *
    * @param roleFunction
    * @return 影响条件数
    */
    int update(RoleFunction roleFunction);

}
