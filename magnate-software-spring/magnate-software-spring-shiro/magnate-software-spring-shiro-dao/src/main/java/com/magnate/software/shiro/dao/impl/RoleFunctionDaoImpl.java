package com.magnate.software.shiro.dao.impl;

import com.magnate.software.shiro.domain.po.RoleFunctionKey;
import com.magnate.software.shiro.dao.RoleFunctionDao;
import com.magnate.software.shiro.mapper.RoleFunctionMapper;
import com.magnate.software.shiro.domain.po.RoleFunction;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
* 角色权限表数据操作实现
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
@Service("roleFunctionDao")
public class RoleFunctionDaoImpl implements RoleFunctionDao {

    @Resource
    private RoleFunctionMapper roleFunctionMapper;

    @Override
    public RoleFunction retrieve(RoleFunctionKey key) {
        return roleFunctionMapper.selectByPrimaryKey(key);
    }

    @Override
    public List<RoleFunction> retrieve(RoleFunction roleFunction) {
        return roleFunctionMapper.select(roleFunction);
    }

    @Override
    public int delete(RoleFunctionKey key) {
        return roleFunctionMapper.deleteByPrimaryKey(key);
    }

    @Override
    public int delete(RoleFunction roleFunction) {
        return roleFunctionMapper.delete(roleFunction);
    }

    @Override
    public int create(RoleFunction roleFunction) {
        roleFunction.setCreateTime(new Date());
        return roleFunctionMapper.insert(roleFunction);
    }

    @Override
    public int update(RoleFunction roleFunction) {
        return roleFunctionMapper.update(roleFunction);
    }
}
