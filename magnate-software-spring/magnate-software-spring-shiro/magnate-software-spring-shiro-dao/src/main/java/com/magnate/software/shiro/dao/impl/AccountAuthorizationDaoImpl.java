package com.magnate.software.shiro.dao.impl;

import com.magnate.software.shiro.domain.po.AccountAuthorizationKey;
import com.magnate.software.shiro.dao.AccountAuthorizationDao;
import com.magnate.software.shiro.mapper.AccountAuthorizationMapper;
import com.magnate.software.shiro.domain.po.AccountAuthorization;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
* 账户授权表数据操作实现
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
@Service("accountAuthorizationDao")
public class AccountAuthorizationDaoImpl implements AccountAuthorizationDao {

    @Resource
    private AccountAuthorizationMapper accountAuthorizationMapper;

    @Override
    public AccountAuthorization retrieve(AccountAuthorizationKey key) {
        return accountAuthorizationMapper.selectByPrimaryKey(key);
    }

    @Override
    public List<AccountAuthorization> retrieve(AccountAuthorization accountAuthorization) {
        return accountAuthorizationMapper.select(accountAuthorization);
    }

    @Override
    public int delete(AccountAuthorizationKey key) {
        return accountAuthorizationMapper.deleteByPrimaryKey(key);
    }

    @Override
    public int delete(AccountAuthorization accountAuthorization) {
        return accountAuthorizationMapper.delete(accountAuthorization);
    }

    @Override
    public int create(AccountAuthorization accountAuthorization) {
        accountAuthorization.setCreateTime(new Date());
        return accountAuthorizationMapper.insert(accountAuthorization);
    }

    @Override
    public int update(AccountAuthorization accountAuthorization) {
        return accountAuthorizationMapper.update(accountAuthorization);
    }
}
