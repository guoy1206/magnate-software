package com.magnate.software.shiro.dao.impl;

import com.magnate.software.shiro.dao.CodeClassDao;
import com.magnate.software.shiro.domain.enums.CodeConstants;
import com.magnate.software.shiro.domain.po.CodeClass;
import com.magnate.software.shiro.mapper.CodeClassMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * 字典分类表数据操作实现
 *
 * @author: WangYang
 * @version: 1.0
 * @datetime: 2016-01-14 17:21:45
 */
@Service("codeClassDao")
public class CodeClassDaoImpl implements CodeClassDao {

    @Resource
    private CodeClassMapper codeClassMapper;

    @Override
    public CodeClass retrieve(String codeClassId) {
        return codeClassMapper.selectByPrimaryKey(codeClassId);
    }

    @Override
    public List<CodeClass> retrieve(CodeClass codeClass) {
        return codeClassMapper.select(codeClass);
    }

    @Override
    public int delete(String codeClassId) {
        return codeClassMapper.deleteByPrimaryKey(codeClassId);
    }

    @Override
    public int delete(CodeClass codeClass) {
        return codeClassMapper.delete(codeClass);
    }

    @Override
    public int create(CodeClass codeClass) {
        codeClass.setCodeClassStatus(CodeConstants.CODE_CLASS_STATUS_NORMAL.getCode());
        codeClass.setCreateTime(new Date());
        return codeClassMapper.insert(codeClass);
    }

    @Override
    public int update(CodeClass codeClass) {
        codeClass.setUpdateTime(new Date());
        return codeClassMapper.update(codeClass);
    }
}
