package com.magnate.software.shiro.mapper;

import com.magnate.software.shiro.domain.po.AccountAuthorization;
import com.magnate.software.shiro.domain.po.AccountAuthorizationKey;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
* 账户授权表数据库操作接口
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
@Repository("accountAuthorizationMapper")
public interface AccountAuthorizationMapper {

    /**
    *
    * 根据主键查询AccountAuthorization对象
    *
    * @param key
    * @return AccountAuthorization对象
    */
    AccountAuthorization selectByPrimaryKey(AccountAuthorizationKey key);

    /**
    *
    * 根据查询条件查询符合条件的AccountAuthorization对象
    *
    * @param accountAuthorization
    * @return 符合条件的AccountAuthorization对象List
    */
    List<AccountAuthorization> select(AccountAuthorization accountAuthorization);

    /**
    *
    * 根据主键删除AccountAuthorization对象
    *
    * @param key
    * @return 影响条件数
    */
    int deleteByPrimaryKey(AccountAuthorizationKey key);

    /**
    *
    * 根据条件删除符合条件的AccountAuthorization对象
    *
    * @param accountAuthorization
    * @return 影响条件数
    */
    int delete(AccountAuthorization accountAuthorization);

    /**
    *
    * 插入AccountAuthorization对象
    *
    * @param accountAuthorization
    * @return 影响条件数
    */
    int insert(AccountAuthorization accountAuthorization);

    /**
    *
    * 更新AccountAuthorization对象
    *
    * @param accountAuthorization
    * @return 影响条件数
    */
    int update(AccountAuthorization accountAuthorization);

}
