package com.magnate.software.shiro.dao.impl;

import com.magnate.software.shiro.dao.FunctionDao;
import com.magnate.software.shiro.domain.enums.CodeConstants;
import com.magnate.software.shiro.mapper.FunctionMapper;
import com.magnate.software.shiro.domain.po.Function;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 功能权限表数据操作实现
 *
 * @author: WangYang
 * @version: 1.0
 * @datetime: 2016-01-14 17:21:45
 */
@Service("functionDao")
public class FunctionDaoImpl implements FunctionDao {

    @Resource
    private FunctionMapper functionMapper;

    @Override
    public Function retrieve(String functionCode) {
        return functionMapper.selectByPrimaryKey(functionCode);
    }

    @Override
    public List<Function> retrieve(Function function) {
        return functionMapper.select(function);
    }

    @Override
    public int delete(String functionCode) {
        return functionMapper.deleteByPrimaryKey(functionCode);
    }

    @Override
    public int delete(Function function) {
        return functionMapper.delete(function);
    }

    @Override
    public int create(Function function) throws Exception {
        function.setFunctionStatus(CodeConstants.FUNCTION_STATUS_NORMAL.getCode());
        function.setCreateTime(new Date());
        return functionMapper.insert(function);
    }

    @Override
    public int update(Function function) throws Exception {
        function.setUpdateTime(new Date());
        return functionMapper.update(function);
    }

    @Override
    public List<Function> queryPermissions(Map<String, Object> params) {
        return functionMapper.selectPermissions(params);
    }
}
