package com.magnate.software.shiro.domain.po;

import java.util.Date;

/**
* 字典表
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*/
public class Code extends CodeKey {

    // 字典名称
    private String codeName;
    // 字典父级
    private Integer codeSuperior;
    // 描述
    private String description;
    // 建立时间
    private Date createTime;
    // 更新时间
    private Date updateTime;
    // 状态
    private Integer codeStatus;

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public Integer getCodeSuperior() {
        return codeSuperior;
    }

    public void setCodeSuperior(Integer codeSuperior) {
        this.codeSuperior = codeSuperior;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(Integer codeStatus) {
        this.codeStatus = codeStatus;
    }

}
