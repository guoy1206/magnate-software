package com.magnate.software.shiro.domain.po;

import java.util.Date;

/**
* 功能权限表
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*/
public class Function  {

    // 功能权限编码
    private String functionCode;
    // 名称
    private String functionName;
    // 类型
    private Integer functionType;
    // 链接
    private String functionLink;
    // 上级ID
    private String functionSuperior;
    // 状态
    private Integer functionStatus;
    // 排序
    private Integer orderId;
    // 是否记录日志
    private Integer isLog;
    // 建立时间
    private Date createTime;
    // 更新时间
    private Date updateTime;

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public Integer getFunctionType() {
        return functionType;
    }

    public void setFunctionType(Integer functionType) {
        this.functionType = functionType;
    }

    public String getFunctionLink() {
        return functionLink;
    }

    public void setFunctionLink(String functionLink) {
        this.functionLink = functionLink;
    }

    public String getFunctionSuperior() {
        return functionSuperior;
    }

    public void setFunctionSuperior(String functionSuperior) {
        this.functionSuperior = functionSuperior;
    }

    public Integer getFunctionStatus() {
        return functionStatus;
    }

    public void setFunctionStatus(Integer functionStatus) {
        this.functionStatus = functionStatus;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getIsLog() {
        return isLog;
    }

    public void setIsLog(Integer isLog) {
        this.isLog = isLog;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }
}
