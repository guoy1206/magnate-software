package com.magnate.software.shiro.domain.po;

import java.util.Date;

/**
* 角色表
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*/
public class Role  {

    // 角色编码
    private String roleCode;
    // 角色名称
    private String roleName;
    // 状态
    private Integer roleStatus;
    // 建立时间
    private Date createTime;
    // 更新时间
    private Date updateTime;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getRoleStatus() {
        return roleStatus;
    }

    public void setRoleStatus(Integer roleStatus) {
        this.roleStatus = roleStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
}
