package com.magnate.software.shiro.domain.po;

/**
* 字典表复合主键
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
public class CodeKey {

    // 分类ID
    private String codeClassId;
    // 字典ID
    private Integer codeId;

    public String getCodeClassId() {
        return codeClassId;
    }

    public void setCodeClassId(String codeClassId) {
        this.codeClassId = codeClassId;
    }

    public Integer getCodeId() {
        return codeId;
    }

    public void setCodeId(Integer codeId) {
        this.codeId = codeId;
    }

}