package com.magnate.software.shiro.domain.enums;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/12 16:08
 */
public enum CodeConstants {

    ACCOUNT_STATUS_NORMAL(1, "账户状态：正常"),
    AUTHORIZATION_TYPE_ROLE(1, "授权类型：角色"),
    AUTHORIZATION_TYPE_ORGANIZATION(2, "授权类型：机构"),
    AUTHORIZATION_TYPE_GROUP(3, "授权类型：分组"),
    ROLE_STATUS_NORMAL(1, "角色状态：正常"),
    FUNCTION_TYPE_MENU(1, "功能权限类型：菜单"),
    FUNCTION_TYPE_ACTION(2, "功能权限类型：动作"),
    FUNCTION_STATUS_NORMAL(1, "功能权限状态：正常"),
    CODE_CLASS_STATUS_NORMAL(1, "字典分类状态：正常"),
    CODE_STATUS_NORMAL(1, "字典状态：正常"),
    ;

    private Integer code;
    private String desc;

    CodeConstants(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
