package com.magnate.software.shiro.domain.enums;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/12 15:58
 */
public enum PropertiesConstants {

    PASSWORD_SALT("password_salt", "密码加密盐"),;

    // 配置文件中的名称
    private String code;
    // 说明
    private String desc;

    PropertiesConstants(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
