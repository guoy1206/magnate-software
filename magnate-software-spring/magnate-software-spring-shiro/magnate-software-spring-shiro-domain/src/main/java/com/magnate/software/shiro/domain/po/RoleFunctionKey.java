package com.magnate.software.shiro.domain.po;

/**
* 角色权限表复合主键
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
public class RoleFunctionKey {

    // 角色ID
    private String roleCode;
    // 功能权限ID
    private String functionCode;

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

}