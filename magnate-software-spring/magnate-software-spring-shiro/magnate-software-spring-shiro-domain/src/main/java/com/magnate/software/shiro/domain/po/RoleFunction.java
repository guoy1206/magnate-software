package com.magnate.software.shiro.domain.po;

import java.util.Date;

/**
* 角色权限表
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*/
public class RoleFunction extends RoleFunctionKey {

    // 建立时间
    private Date createTime;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
