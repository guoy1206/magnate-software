package com.magnate.software.shiro.domain.po;

/**
* 账户授权表复合主键
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*
*/
public class AccountAuthorizationKey {

    // 登录账号
    private String account;
    // 授权编码
    private String authorizationCode;
    // 授权类型
    private Integer authorizationType;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public Integer getAuthorizationType() {
        return authorizationType;
    }

    public void setAuthorizationType(Integer authorizationType) {
        this.authorizationType = authorizationType;
    }

}