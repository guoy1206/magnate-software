package com.magnate.software.shiro.domain.po;

import java.util.Date;

/**
* 字典分类表
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-01-14 17:21:45
*/
public class CodeClass  {

    // 分类ID
    private String codeClassId;
    // 类型名称
    private String codeClassName;
    // 描述
    private String description;
    // 建立时间
    private Date createTime;
    // 更新时间
    private Date updateTime;
    // 状态
    private Integer codeClassStatus;

    public String getCodeClassName() {
        return codeClassName;
    }

    public void setCodeClassName(String codeClassName) {
        this.codeClassName = codeClassName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCodeClassStatus() {
        return codeClassStatus;
    }

    public void setCodeClassStatus(Integer codeClassStatus) {
        this.codeClassStatus = codeClassStatus;
    }

    public String getCodeClassId() {
        return codeClassId;
    }

    public void setCodeClassId(String codeClassId) {
        this.codeClassId = codeClassId;
    }
}
