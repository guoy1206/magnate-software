SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tb_account`
-- ----------------------------
DROP TABLE IF EXISTS `tb_account`;
CREATE TABLE `tb_account` (
  `account` varchar(50) NOT NULL COMMENT '登录账号',
  `password` varchar(50) NOT NULL COMMENT '登录密码',
  `account_status` smallint(5) unsigned NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT '建立时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账户表';

-- ----------------------------
-- Records of tb_account
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_account_authorization`
-- ----------------------------
DROP TABLE IF EXISTS `tb_account_authorization`;
CREATE TABLE `tb_account_authorization` (
  `account` varchar(50) NOT NULL COMMENT '登录账号',
  `authorization_code` varchar(50) NOT NULL COMMENT '授权编码',
  `authorization_type` smallint(5) unsigned NOT NULL COMMENT '授权类型',
  `create_time` datetime NOT NULL COMMENT '建立时间',
  PRIMARY KEY (`account`,`authorization_code`,`authorization_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账户授权表';

-- ----------------------------
-- Records of tb_account_authorization
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_code`
-- ----------------------------
DROP TABLE IF EXISTS `tb_code`;
CREATE TABLE `tb_code` (
  `code_class_id` varchar(50) NOT NULL COMMENT '分类ID',
  `code_id` smallint(5) unsigned NOT NULL COMMENT '字典ID',
  `code_name` varchar(50) NOT NULL COMMENT '字典名称',
  `code_superior` smallint(5) unsigned DEFAULT NULL COMMENT '字典父级',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `create_time` datetime NOT NULL COMMENT '建立时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `code_status` smallint(5) unsigned NOT NULL COMMENT '状态',
  PRIMARY KEY (`code_class_id`,`code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of tb_code
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_code_class`
-- ----------------------------
DROP TABLE IF EXISTS `tb_code_class`;
CREATE TABLE `tb_code_class` (
  `code_class_id` varchar(50) NOT NULL COMMENT '分类ID',
  `code_class_name` varchar(50) NOT NULL COMMENT '类型名称',
  `description` varchar(100) NOT NULL COMMENT '描述',
  `create_time` datetime NOT NULL COMMENT '建立时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `code_class_status` smallint(5) unsigned NOT NULL COMMENT '状态',
  PRIMARY KEY (`code_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典分类表';

-- ----------------------------
-- Records of tb_code_class
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_function`
-- ----------------------------
DROP TABLE IF EXISTS `tb_function`;
CREATE TABLE `tb_function` (
  `function_code` varchar(50) NOT NULL COMMENT '功能权限编码',
  `function_name` varchar(50) NOT NULL COMMENT '名称',
  `function_type` smallint(5) unsigned NOT NULL COMMENT '类型',
  `function_link` varchar(200) DEFAULT NULL COMMENT '链接',
  `function_superior` varchar(50) DEFAULT NULL COMMENT '上级ID',
  `function_status` smallint(5) unsigned NOT NULL COMMENT '状态',
  `order_id` smallint(5) unsigned DEFAULT NULL COMMENT '排序',
  `is_log` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT '是否记录日志',
  `create_time` datetime NOT NULL COMMENT '建立时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`function_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='功能权限表';

-- ----------------------------
-- Records of tb_function
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_role`
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role` (
  `role_code` varchar(50) NOT NULL COMMENT '角色编码',
  `role_name` varchar(50) NOT NULL COMMENT '角色名称',
  `role_status` smallint(5) unsigned NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT '建立时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of tb_role
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_role_function`
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_function`;
CREATE TABLE `tb_role_function` (
  `role_code` varchar(50) NOT NULL COMMENT '角色ID',
  `function_code` varchar(50) NOT NULL COMMENT '功能权限ID',
  `create_time` datetime NOT NULL COMMENT '建立时间',
  PRIMARY KEY (`role_code`,`function_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限表';

-- ----------------------------
-- Records of tb_role_function
-- ----------------------------

-- 唯一性约束建表语句
-- UNIQUE KEY `uq_function_name` (`function_name`)