<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>综合案例</title>
    <link rel="stylesheet" href="<c:url value="/static/css/layout-default-latest.css" />">
</head>
<body>

<iframe name="content" class="ui-layout-center"
        src="${pageContext.request.contextPath}/welcome" frameborder="0" scrolling="auto"></iframe>
<div class="ui-layout-north">欢迎[<shiro:principal/>]学习综合案例，<a href="${pageContext.request.contextPath}/logout">退出</a></div>
<div class="ui-layout-south">

</div>
<div class="ui-layout-west">
    功能菜单<br/>
    <c:forEach items="${menus}" var="item">
        <a href="<c:url value="${item.functionLink}"/>" target="content">${item.functionName}</a><br/>
    </c:forEach>
</div>


<script src="<c:url value="/static/js/jquery-1.11.0.min.js" />"></script>
<script src="<c:url value="/static/js/jquery.layout-latest.min.js" />"></script>
<script>
    $(function () {
        $(document).ready(function () {
            $('body').layout({ applyDemoStyles: true });
        });
    });
</script>
</body>
</html>