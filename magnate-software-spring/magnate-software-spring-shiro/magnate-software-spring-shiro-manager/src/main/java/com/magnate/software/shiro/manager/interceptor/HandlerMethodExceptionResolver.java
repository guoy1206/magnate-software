package com.magnate.software.shiro.manager.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 控制层异常处理类
 * User: WangYang
 * Date: 13-12-23
 * Time: 下午6:35
 */
@Service("handlerMethodExceptionResolver")
public class HandlerMethodExceptionResolver implements HandlerExceptionResolver {

    private static final String markTag = HandlerMethodExceptionResolver.class.getSimpleName();

    private static Logger logger = LoggerFactory.getLogger(HandlerMethodExceptionResolver.class);

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object o, Exception ex) {
        String message = "方法执行异常！";
        if (ex instanceof DuplicateKeyException) {
            message = "唯一性约束！";
        }
        logger.error(message, ex);
        ModelAndView mv = new ModelAndView();
        mv.addObject("exception", ex);
        mv.setViewName("unauthorized");
        return mv;
    }

}
