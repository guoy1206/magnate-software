package com.magnate.software.shiro.manager.controller;

import com.magnate.software.shiro.domain.enums.CodeConstants;
import com.magnate.software.shiro.domain.po.Account;
import com.magnate.software.shiro.domain.po.Function;
import com.magnate.software.shiro.service.FunctionService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * 账户控制器
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/13 15:57
 */
@Controller
public class SystemController {

    @Resource
    private FunctionService functionService;

    @RequestMapping(value = "/login")
    public String login(@ModelAttribute Account account) {
        return "login";
    }

    @RequestMapping(value = "/index")
    public ModelAndView index() {
        Subject currentAccount = SecurityUtils.getSubject();//获取当前用户
        List<Function> functions = functionService.queryPermissions((String) currentAccount.getPrincipal(), CodeConstants.FUNCTION_TYPE_MENU.getCode());
        ModelAndView mv = new ModelAndView();
        mv.addObject("menus", functions);
        mv.setViewName("index");
        return mv;
    }

    @RequestMapping(value = "/welcome")
    public String welcome(@ModelAttribute Account account) {
        return "welcome";
    }
}
