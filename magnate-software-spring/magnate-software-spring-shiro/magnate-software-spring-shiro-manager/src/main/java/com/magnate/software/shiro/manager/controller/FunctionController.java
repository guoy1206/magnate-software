package com.magnate.software.shiro.manager.controller;

import com.magnate.software.shiro.domain.po.Function;
import com.magnate.software.shiro.domain.po.Role;
import com.magnate.software.shiro.service.FunctionService;
import com.magnate.software.shiro.service.RoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 账户控制器
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/13 15:57
 */
@RestController
@RequestMapping("/function")
public class FunctionController {

    @Resource
    private FunctionService functionService;

    @RequiresPermissions("AC020104")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public List<Function> list(@ModelAttribute Function function) {
        return functionService.query(function);
    }

    /**
     * 添加功能权限
     *
     * @param function 功能权限
     * @return
     * @throws Exception
     */
    @RequiresPermissions("AC020101")
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Function create(@ModelAttribute Function function) throws Exception {
        Function result = functionService.create(function);
        return result;
    }

}
