package com.magnate.software.shiro.manager.controller;

import com.magnate.software.shiro.domain.po.Account;
import com.magnate.software.shiro.domain.po.AccountAuthorization;
import com.magnate.software.shiro.service.AccountService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 账户控制器
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/13 15:57
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    @Resource
    private AccountService accountService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> list(@ModelAttribute Account account) {
        return accountService.query(account);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Account create(@ModelAttribute Account account) {
        Account result = accountService.create(account);
        return result;
    }

    @RequiresPermissions("AC010105")
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public void authorization(@ModelAttribute AccountAuthorization authorization) {
        accountService.authorization(authorization);
    }

}
