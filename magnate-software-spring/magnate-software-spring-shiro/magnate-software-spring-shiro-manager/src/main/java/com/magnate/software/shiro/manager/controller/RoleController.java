package com.magnate.software.shiro.manager.controller;

import com.magnate.software.shiro.domain.po.Role;
import com.magnate.software.shiro.domain.po.RoleFunction;
import com.magnate.software.shiro.service.RoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 账户控制器
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/13 15:57
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Resource
    private RoleService roleService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public List<Role> list(@ModelAttribute Role role) {
        return roleService.query(role);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Role create(@ModelAttribute Role role) {
        Role result = roleService.create(role);
        return result;
    }

    @RequiresPermissions("AC030105")
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public void authorization(@ModelAttribute RoleFunction authorization) {
        roleService.authorization(authorization);
    }

}
