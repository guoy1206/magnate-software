package com.magnate.software.shiro.manager.test;

import com.magnate.software.common.utils.HttpClientUtils;
import com.magnate.software.shiro.domain.enums.CodeConstants;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色服务单元测试
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/14 10:45
 */
@FixMethodOrder(MethodSorters.DEFAULT)
public class RoleControllerTest {

    private static final String serverUrl = "http://192.168.2.69:8090/bms-api-app";
    private static final String localhostUrl = "http://127.0.0.1:8080/shiro";

    @Test
    public void createRoleTest() {
        try {
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("roleCode", "ROLE_ADMIN"));
            params.add(new BasicNameValuePair("roleName", "管理员"));
            HttpClient httpClient = HttpClientUtils.getCustomerHttpClient();
            HttpPost httpPost = new HttpPost(localhostUrl + "/role");
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8")); //将参数填入POST Entity中
            HttpResponse httpResponse = httpClient.execute(httpPost);
            System.out.println("HTTP Status: " + httpResponse.getStatusLine().getStatusCode());
            System.out.println("HTTP ReasonPhrase: " + EntityUtils.toString(httpResponse.getEntity(), "utf-8"));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Test
    public void authorizationTest() {
        try {
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("roleCode", "ROLE_ADMIN"));
            params.add(new BasicNameValuePair("functionCode", "MU020101"));
            HttpClient httpClient = HttpClientUtils.getCustomerHttpClient();
            HttpPost httpPost = new HttpPost(localhostUrl + "/role/auth");
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8")); //将参数填入POST Entity中
            HttpResponse httpResponse = httpClient.execute(httpPost);
            System.out.println("HTTP Status: " + httpResponse.getStatusLine().getStatusCode());
            System.out.println("HTTP ReasonPhrase: " + EntityUtils.toString(httpResponse.getEntity(), "utf-8"));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

}
