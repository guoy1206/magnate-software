package com.magnate.software.spring.core.test;

import com.magnate.software.domain.po.User;
import com.magnate.software.spring.core.event.CustomEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/6/21 14:44
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/spring-config.xml")
public class CustomEventTest {

    @Resource
    private ApplicationContext applicationContext;

    @Test
    public void eventTest() {
        User user = new User();
        user.setName("张三");
        user.setPhone("1234567890");
        applicationContext.publishEvent(new CustomEvent(user));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
