package com.magnate.software.spring.core.event;

import com.magnate.software.domain.po.User;
import org.springframework.context.ApplicationEvent;

/**
 * 自定义事件
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/6/21 14:32
 */
public class CustomEvent extends ApplicationEvent {

    private User source;

    public CustomEvent(User source) {
        super(source);
        this.source = source;
    }

    public User getSource() {
        return source;
    }

    public void setSource(User source) {
        this.source = source;
    }
}
