package com.magnate.software.spring.core.event;

import com.magnate.software.domain.po.User;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Service;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/6/21 14:39
 */
@Service("customSmartListener")
public class CustomSmartListener implements SmartApplicationListener {

    @Override
    public boolean supportsEventType(Class<? extends ApplicationEvent> eventType) {
        return eventType == CustomEvent.class;
    }

    @Override
    public boolean supportsSourceType(Class<?> sourceType) {
        return sourceType == User.class;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        System.out.println("ORDER 1 自定义事件驱动：" + ((User) event.getSource()).getName());
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
