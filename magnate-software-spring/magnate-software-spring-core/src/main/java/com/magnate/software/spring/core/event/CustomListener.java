package com.magnate.software.spring.core.event;

import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * class description
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/6/21 14:39
 */
@Service("customListener")
public class CustomListener implements ApplicationListener<CustomEvent> {

    @Async
    @Override
    public void onApplicationEvent(CustomEvent event) {
        System.out.println("自定义事件驱动：" + event.getSource().getName());
    }

}
