package com.magnate.software.mongo.db.test;

import com.magnate.software.common.utils.StringUtils;
import com.magnate.software.mongo.db.context.PagingContextHolder;
import com.magnate.software.mongo.db.dao.UserMongoDao;
import com.magnate.software.mongo.db.domain.Field;
import com.magnate.software.mongo.db.domain.Page;
import com.magnate.software.mongo.db.domain.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/11/28 16:10
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/spring-config.xml")
public class MongoDBTest {

    private static final String CollectionName = "user";

    private static Logger logger = LoggerFactory.getLogger(MongoDBTest.class);

    @Resource
    private UserMongoDao userMongoDao;

    private User initUser() {
        User user = new User();
        user.setName(StringUtils.randomUUID());
        user.setAge(new Random().nextInt(100));
        user.setCompany(getString(1));
        return user;
    }

    private String getString(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(generateZh(4));
        }
        return sb.toString();
    }

    private int nextInt(int min, int max) {
        return new Random().nextInt(max - min) + min;
    }

    public String generateZh(int length) {
        char[] captcha = new char[length];
        for (int i = 0; i < length; i++) {
            captcha[i] = (char) nextInt(0x53E3, 0x559D);
        }
        return new String(captcha);
    }

    @Test
    public void saveTest() {
        for (int i = 0; i < 50; i++) {
            userMongoDao.save(CollectionName, initUser());
        }
    }


    @Test
    public void getTest() {
        Field key = new Field();
        key.setName("name");
        key.setValue("2434496b39104cc28cfd1cb2c4b16d5d");
        User user = userMongoDao.get(CollectionName, key);
        logger.info(user.getName() + " " + user.getCompany() + " " + user.getAge());
        Assert.assertTrue(user.getCompany().equals("部门名称"));
    }

    @Test
    public void getsTest() {
        List<User> users = userMongoDao.gets(CollectionName, null);
        Page page = PagingContextHolder.getPage();
        logger.info(users.get(0).getName() + " " + users.get(0).getCompany() + " " + users.get(0).getAge());
        //Assert.assertTrue(users.size() == 2);
    }

    @Test
    public void updateTest() {
        List<Field> where = new ArrayList<Field>();
        Field key = new Field();
        key.setName("name");
        key.setValue("2434496b39104cc28cfd1cb2c4b16d5d");
        where.add(key);

        List<Field> data = new ArrayList<Field>();
        Field update = new Field();
        update.setName("company");
        update.setValue("部门名称");
        data.add(update);

        userMongoDao.update(CollectionName, where, data);
    }

    @Test
    public void removeTest() {
        List<Field> where = new ArrayList<Field>();
        Field key = new Field();
        key.setName("name");
        key.setValue("502594bf328142f980b1d01b8b0d48a1");
        where.add(key);

        userMongoDao.remove(CollectionName, where);
    }

    public static void main(String[] args) {
        MongoDBTest test = new MongoDBTest();
        System.out.println(test.getString(1));
    }

}
