package com.magnate.software.mongo.db.dao;

import com.magnate.software.mongo.db.domain.Field;

import java.util.List;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/11/25 17:36
 */
public interface MongoDao<T> {

    /**
     * 保存信息至数据库
     *
     * @param collectionName 文档集合名称
     * @param object         内容
     */
    void save(String collectionName, T object);

    /**
     * 获取对象体合
     *
     * @param collectionName 文档集合名称
     * @param key            主键集合
     * @param params         参数集合
     * @return
     */
    void update(String collectionName, List<Field> key, List<Field> params);

    /**
     * 根据主键删除单一对象
     *
     * @param collectionName 文档集合名称
     * @param key            主键
     */
    void remove(String collectionName, Field key);

    /**
     * 根据参数删除对象
     *
     * @param collectionName 文档集合名称
     * @param params         参数
     */
    void remove(String collectionName, List<Field> params);

    /**
     * 获取单一对象
     *
     * @param collectionName 文档集合名称
     * @param key            主键
     * @return
     */
    T get(String collectionName, Field key);

    /**
     * 获取单一对象
     *
     * @param collectionName 文档集合名称
     * @param key            主键集合
     * @return
     */
    T get(String collectionName, List<Field> key);

    /**
     * 获取对象体合
     *
     * @param collectionName 文档集合名称
     * @param params         参数
     * @return
     */
    List<T> gets(String collectionName, List<Field> params);
}
