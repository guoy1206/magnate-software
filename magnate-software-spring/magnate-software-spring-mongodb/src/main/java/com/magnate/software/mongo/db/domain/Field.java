package com.magnate.software.mongo.db.domain;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/11/25 17:43
 */
public class Field {

    // 名称
    private String name;
    // 值
    private Object value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
