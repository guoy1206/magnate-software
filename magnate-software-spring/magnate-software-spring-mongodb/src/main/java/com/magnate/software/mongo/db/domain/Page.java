package com.magnate.software.mongo.db.domain;

public class Page {
    //每页显示记录数
    private long pageSize;
    //总页数
    private long totalPage;
    //总记录数
    private long totalResult;
    //当前页
    private long currentPage;
    //当前记录起始行
    private long currentResult;

    /**
     *  获得记录分页总页数
     * @return 总页数
     */
    public long getTotalPage() {
        if (totalResult % pageSize == 0) {
            totalPage = totalResult / pageSize;
        } else {
            totalPage = totalResult / pageSize + 1;
        }
        return totalPage;
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }

    public long getTotalResult() {
        return totalResult;
    }

    public void setTotalResult(long totalResult) {
        this.totalResult = totalResult;
    }

    public long getCurrentPage() {
        if (currentPage <= 0) {
            currentPage = 1;
        }
        if (currentPage > getTotalPage()) {
            currentPage = getTotalPage();
        }
        return currentPage;
    }

    public void setCurrentPage(long currentPage) {
        this.currentPage = currentPage;
    }

    public long getCurrentResult() {
        currentResult = (getCurrentPage() - 1) * getPageSize();
        if (currentResult < 0) {
            currentResult = 0;
        }
        return currentResult;
    }

    public void setCurrentResult(long currentResult) {
        this.currentResult = currentResult;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }
}
