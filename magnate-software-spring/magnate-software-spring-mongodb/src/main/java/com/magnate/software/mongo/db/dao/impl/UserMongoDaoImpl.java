package com.magnate.software.mongo.db.dao.impl;

import com.magnate.software.mongo.db.context.PagingContextHolder;
import com.magnate.software.mongo.db.dao.UserMongoDao;
import com.magnate.software.mongo.db.domain.Field;
import com.magnate.software.mongo.db.domain.Page;
import com.magnate.software.mongo.db.domain.User;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/11/25 17:51
 */
@Service("userMongoDao")
public class UserMongoDaoImpl implements UserMongoDao {

    @Resource
    private MongoTemplate mongoTemplate;

    @Override
    public void save(String collectionName, User object) {
        mongoTemplate.insert(object, collectionName);
    }

    @Override
    public void update(String collectionName, List<Field> key, List<Field> params) {
        Query query = new Query();
        for (Field item : key) {
            query.addCriteria(Criteria.where(item.getName()).is(item.getValue()));
        }

        Update update = new Update();
        for (Field item : params) {
            update.set(item.getName(), item.getValue());
        }

        mongoTemplate.upsert(query, update, User.class, collectionName);
    }

    @Override
    public void remove(String collectionName, Field key) {
        mongoTemplate.remove(new Query(Criteria.where(key.getName()).is(key.getValue())), User.class, collectionName);
    }

    @Override
    public void remove(String collectionName, List<Field> params) {
        Query query = new Query();
        for (Field item : params) {
            query.addCriteria(Criteria.where(item.getName()).is(item.getValue()));
        }
        mongoTemplate.remove(query, User.class, collectionName);
    }

    @Override
    public User get(String collectionName, Field key) {
        return mongoTemplate.findOne(new Query(Criteria.where(key.getName()).is(key.getValue())), User.class, collectionName);
    }

    @Override
    public User get(String collectionName, List<Field> key) {
        Query query = new Query();
        for (Field item : key) {
            query.addCriteria(Criteria.where(item.getName()).is(item.getValue()));
        }

        return mongoTemplate.findOne(query, User.class, collectionName);
    }

    @Override
    public List<User> gets(String collectionName, List<Field> params) {
        Page page = new Page();
        page.setCurrentPage(1l);
        page.setPageSize(10);
        PagingContextHolder.setPage(page);

        Query query = new Query();
        if (CollectionUtils.isNotEmpty(params)) {
            for (Field item : params) {
                query.addCriteria(Criteria.where(item.getName()).is(item.getValue()));
            }
        }

        return mongoTemplate.find(query, User.class, collectionName);
    }

}
