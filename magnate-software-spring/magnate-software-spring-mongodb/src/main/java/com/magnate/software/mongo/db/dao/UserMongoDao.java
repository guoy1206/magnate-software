package com.magnate.software.mongo.db.dao;

import com.magnate.software.mongo.db.domain.User;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/11/25 17:49
 */
public interface UserMongoDao extends MongoDao<User> {
}
