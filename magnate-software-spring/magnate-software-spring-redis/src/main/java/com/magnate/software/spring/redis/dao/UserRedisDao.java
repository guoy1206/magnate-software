package com.magnate.software.spring.redis.dao;

import com.magnate.software.domain.po.User;

import java.util.List;

/**
 * 用户Redis操作接口
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/9/29 16:40
 */
public interface UserRedisDao {

    /**
     * 保存用户
     *
     * @param user 用户
     * @return
     */
    boolean save(User user);

    /**
     * 根据手机号查找用户
     *
     * @param phone 手机号
     * @return
     */
    User getUser(String phone);

    /**
     * 根据条件查询用户列表
     *
     * @param phones 查询条件
     * @return
     */
    List<User> getUsers(List<String> phones);

    /**
     * 更新用户
     *
     * @param user 用户
     * @return
     */
    boolean update(User user);

    /**
     * 根据手机号删除用户
     *
     * @param phone 手机号
     * @return
     */
    boolean delete(String phone);

}
