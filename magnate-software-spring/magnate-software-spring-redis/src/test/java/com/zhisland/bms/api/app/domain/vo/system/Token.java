package com.zhisland.bms.api.app.domain.vo.system;

import java.io.Serializable;

/**
 * TOKEN凭证类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/10/27 12:31
 */
public class Token implements Serializable {

    private static final long serialVersionUID = -5888707740021483901L;

    private String accessToken; // 访问TOKEN
    private String refreshToken; // 刷新TOKEN
    private long expiresIn;      //access token 有效期 的时间点 单位秒
    private long createTime; // 建立时间
    private Long userId; // 用户ID

    public Token() {
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
