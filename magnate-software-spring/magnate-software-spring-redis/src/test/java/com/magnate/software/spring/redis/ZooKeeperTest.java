package com.magnate.software.spring.redis;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.api.CreateBuilder;
import org.apache.curator.utils.CloseableUtils;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Id;
import org.apache.zookeeper.server.auth.DigestAuthenticationProvider;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * ZooKeeper测试用例
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/2/23 10:28
 */
public class ZooKeeperTest {

    private static String connectString = "192.168.2.91:2181";

    private static CuratorFramework client = ClientFactory.createSimple(connectString);

    public static void main(String[] args) {
        try {
            client.start();
            //System.out.println(new String(client.getData().forPath("/test"), "UTF-8"));
            List<ACL> acls = new ArrayList<ACL>(2);

            Id admin = new Id("digest", DigestAuthenticationProvider.generateDigest("admin:admin123"));
            ACL adminAcl = new ACL(ZooDefs.Perms.ALL, admin);

            Id read = new Id("digest", DigestAuthenticationProvider.generateDigest("guest:guest123"));
            ACL readAcl = new ACL(ZooDefs.Perms.READ, read);

            acls.add(adminAcl);
            acls.add(readAcl);

            CreateBuilder createBuilder = client.create();
            createBuilder.withACL(acls);
            createBuilder.withMode(CreateMode.PERSISTENT);
            createBuilder.creatingParentsIfNeeded().forPath("/test", "hello world".getBytes());
//            client.setData().forPath("/test", "hello world setData".getBytes());
//            client.delete().forPath("/test");
            //this.connect("192.168.2.91:2181");
            //this.zooKeeper.create("/test", "this is test".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            CloseableUtils.closeQuietly(client);
        }
    }

    public void zooKeeperTest() {
        /*
        CuratorFramework client = CuratorFrameworkFactory.builder().connectString("192.168.2.91:2181")
                .sessionTimeoutMs(30000)
                .connectionTimeoutMs(30000)
                .canBeReadOnly(false)
                .retryPolicy(new ExponentialBackoffRetry(1000, Integer.MAX_VALUE))
                .namespace("test")
                .defaultData(null)
                .build();
*/
        // 启动 上面的namespace会作为一个最根的节点在使用时自动创建

        try {
            client.start();
            //System.out.println(new String(client.getData().forPath("/test"), "UTF-8"));
            client.create().forPath("/test2", "hello world".getBytes());
            CloseableUtils.closeQuietly(client);
            //client.delete().inBackground().forPath("/test");
            //this.connect("192.168.2.91:2181");
            //this.zooKeeper.create("/test", "this is test".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
