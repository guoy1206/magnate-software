package com.magnate.software.spring.redis;

import com.magnate.software.domain.po.User;
import com.magnate.software.spring.redis.dao.UserRedisDao;
import com.magnate.software.spring.redis.dao.serializer.MsgPackRedisSerializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.msgpack.MessagePack;
import org.msgpack.type.Value;
import org.msgpack.unpacker.Converter;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;

/**
 * Created by WangYang on 2015/9/29.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/spring-config.xml")
public class UserRedisMsgPackTest {

    private static final String KEY = "wy:user:{0}";

    @Resource
    private RedisTemplate msgPackRedisTemplate;

    private User user;

    @Before
    public void init() {
        user = new User();
        user.setName("redis_name_1");
        user.setPhone("12345678901");
    }

    @Test
    public void saveTest() {
        msgPackRedisTemplate.opsForValue().set(MessageFormat.format(KEY, user.getPhone()), user);
    }

    @Test
    public void getTest() {
        User tempUser = (User) msgPackRedisTemplate.opsForValue().get(MessageFormat.format(KEY, "12345678901"));
        Assert.assertEquals("redis_name_1", tempUser.getName());
    }

}
