package com.magnate.software.spring.redis;

import com.magnate.software.domain.po.User;
import com.magnate.software.spring.redis.dao.UserRedisDao;
import com.magnate.software.spring.redis.dao.serializer.MsgPackRedisSerializer;
import javafx.scene.chart.XYChart;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.msgpack.MessagePack;
import org.msgpack.type.Value;
import org.msgpack.unpacker.Converter;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;

/**
 * Created by WangYang on 2015/9/29.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/spring-config.xml")
public class UserRedisDaoTest {

    private static final String KEY = "wy:user:{0}";

    private static final String TOKEN_PRE = "";

    @Resource
    private UserRedisDao userRedisDao;

    @Resource
    private RedisTemplate redisTemplate;

    private User user;

    @Before
    public void init() {
        user = new User();
        user.setName("redis_name_1");
        user.setPhone("12345678901");
    }

    @Test
    public void saveTest() {
        Assert.assertTrue(userRedisDao.save(user));
    }

    @Test
    public void getTest() {
        User temp = userRedisDao.getUser("12345678901");
        System.out.println(temp.getName());
        Assert.assertEquals(temp.getName(), user.getName());
    }

    @Test
    public void zSetTest() throws Exception {

        Feed feed = new Feed();
        feed.setId(1473213093751012345L);
        feed.setKey("123_123");
        feed.setRead(0);


        BoundValueOperations<String, Feed> operations = redisTemplate.boundValueOps(KEY + "ZSET_FEED");
        operations.set(feed);
    }

    @Test
    public void getValuesTest() {
        try {
            MsgPackRedisSerializer<Feed> msgPackRedisSerializer = new MsgPackRedisSerializer<Feed>();
            msgPackRedisSerializer.setClassType(Feed.class);
            // redisTemplate.setValueSerializer(msgPackRedisSerializer);

            BoundValueOperations<String, Value> operations = redisTemplate.boundValueOps(KEY + "ZSET_FEED");
            System.out.println(operations.get());
            Value value = operations.get();
            Feed feed = new Converter(value).read(Feed.class);
            System.out.println(feed.getKey());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Set set = redisTemplate.opsForZSet().reverseRange("BMS_API_APP_FEED_INDEX_6171129651931381767",
        //        0,
        //        redisTemplate.opsForZSet().size("BMS_API_APP_FEED_INDEX_6171129651931381767"));

    }

    @Test
    public void deleteValuesTest() {
        redisTemplate.delete(KEY + "ZSET_FEED");
        //Set set = redisTemplate.opsForZSet().reverseRange("BMS_API_APP_FEED_INDEX_6171129651931381767",
        //        0,
        //        redisTemplate.opsForZSet().size("BMS_API_APP_FEED_INDEX_6171129651931381767"));

    }


    @Test
    public void removeZSetTest() {
        redisTemplate.opsForZSet().removeRange(KEY + "ZSET", 0, redisTemplate.opsForZSet().size(KEY + "ZSET"));
    }

    @Test
    public void getUserToken() {
        BoundListOperations operations = redisTemplate.boundListOps(KEY + "ZSET_1_11");
        operations.remove(4, "123_");

        //BoundValueOperations<String, Token> bvo = redisTemplate.boundValueOps("BMS_API_APP_USER_TOKEN_6175481963902664707");
        //System.out.println(bvo.get().getUserId());
    }

    @Test
    public void msgPackTest() {
        try {
            MessagePack messagePack = new MessagePack();

            Feed feed = new Feed();
            feed.setId(1473213093751012345L);
            feed.setKey("123_123");
            feed.setTime(new Date().getTime());
            feed.setRead(0);

            byte[] bytes = messagePack.write(feed);
            System.out.println(bytes);
            Feed temp = messagePack.read(bytes, Feed.class);
            System.out.println(temp.getKey());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //BoundValueOperations<String, Token> bvo = redisTemplate.boundValueOps("BMS_API_APP_USER_TOKEN_6175481963902664707");
        //System.out.println(bvo.get().getUserId());
    }

}
