package com.magnate.software.spring.redis;

import com.magnate.software.domain.po.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.BoundZSetOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/9/27 17:42
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/spring-config.xml")
public class RedisTemplateOperateTest {

    private static final String KEY = "BMS_API_APP_FEED_";

    private static final String FEED_INDEX = KEY + "INDEX_";

    private static final String FEED_INFO = KEY + "INFO_";

    @Resource
    private RedisTemplate stringRedisTemplate;

    @Test
    public void getTest() {
        BoundZSetOperations<String, String> zSetOperations = stringRedisTemplate.boundZSetOps(FEED_INDEX + "6171129651931381767");
        Set<String> userFeedIndex = zSetOperations.range(0, zSetOperations.size());

        RedisSerializer<String> serializer = stringRedisTemplate.getStringSerializer();

        List<Object> feeds = stringRedisTemplate.executePipelined(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection conn)
                    throws DataAccessException {
                for (String item : userFeedIndex) {
                    BoundValueOperations<String, String> valueOperations = stringRedisTemplate.boundValueOps(FEED_INFO + item);
                    System.out.println(valueOperations.get());
                    conn.rPop(valueOperations.get().getBytes());
                }
                return null;
            }
        }, serializer);

        feeds.forEach(System.out::println);

    }

}
