package com.magnate.software.spring.redis;

import org.msgpack.annotation.Message;

import java.io.Serializable;

/**
 * Created by WangYang on 2016/8/29.
 */
@Message
public class Feed implements Serializable {

    private static final long serialVersionUID = -1447342918405631794L;

    private Long id;

    private String key;

    private long time;

    private int read;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }
}
