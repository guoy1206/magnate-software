package com.magnate.software.manager.test;

import com.magnate.software.test.HttpClientUtils;
import com.magnate.software.test.HttpSLLClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 异步控制器测试类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/8 16:16
 */
public class AsyncControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(AsyncControllerTest.class);

    private static final String localUrl = "http://localhost:8080/mvc";

    /**
     * 获取用户印象标签
     *
     * @throws Exception
     */
    @Test
    public void getImpressionUssrTagsTest() throws Exception {
        try {
            HttpClient httpClient = HttpClientUtils.getCustomerHttpClient();
            HttpGet httpGet = new HttpGet(localUrl + "/async/response/body");
            httpGet.setHeader("apiVersion", "1.0");
            HttpResponse httpResponse = httpClient.execute(httpGet);
            String body = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
            logger.info("HTTP Status: " + httpResponse.getStatusLine().getStatusCode());
            logger.info("HTTP ReasonPhrase: " + body);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

}
