package com.magnate.software.manager.interceptor;


import com.magnate.software.common.utils.JacksonObjectMapper;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Spring 方法拦截器
 * User: WangYang
 * Date: 13-4-17
 * Time: 下午5:23
 */
@Service("enableLoggingInterceptor")
public class EnableLoggingInterceptor implements MethodInterceptor {

    private static Logger logger = LoggerFactory.getLogger(EnableLoggingInterceptor.class);

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        try {
            // 方法处理
            Object result = invocation.proceed();
            // 方法处理完成后处理
            logger.info(JacksonObjectMapper.getInstance().writeValueAsString(result));
            return result;
        } catch (Throwable t) {
            // 方法处理完成后处理
            logger.error(t.getMessage());
            throw t;
        }
    }

}
