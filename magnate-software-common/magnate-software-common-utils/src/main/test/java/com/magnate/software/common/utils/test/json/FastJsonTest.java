package com.magnate.software.common.utils.test.json;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

/**
 * 淘宝JSON工具测试类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/4/15 13:44
 */
public class FastJsonTest {

    @Test
    public void jsonTest() {
        UserBean userBean = new UserBean();
        userBean.setName("name");
        System.out.println(JSON.toJSONString(userBean));
    }

    class UserBean {
        private String name;
        private Integer id;
        private Integer sex;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getSex() {
            return sex;
        }

        public void setSex(Integer sex) {
            this.sex = sex;
        }
    }

}
