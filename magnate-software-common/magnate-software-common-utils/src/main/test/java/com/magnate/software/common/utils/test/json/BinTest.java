package com.magnate.software.common.utils.test.json;

import org.junit.Test;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/6 17:02
 */
public class BinTest {

    @Test
    public void binTest() {
        for (int i = 0; i < 5; i++) {
            System.out.println(Math.pow(2, i) + " " + Integer.toBinaryString((int)Math.pow(2, i)));
        }
        System.out.println(Integer.toBinaryString(Integer.parseInt("31")));
    }
}
