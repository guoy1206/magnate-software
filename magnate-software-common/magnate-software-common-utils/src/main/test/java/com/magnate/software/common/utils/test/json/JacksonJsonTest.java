package com.magnate.software.common.utils.test.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/4 17:10
 */
public class JacksonJsonTest {

    private String userJson = "{" +
            "  \"name\" : { \"first\" : \"Joe\", \"last\" : \"Sixpack\" }," +
            "  \"gender\" : \"MALE\"," +
            "  \"verified\" : false," +
            "  \"userImage\" : \"Rm9vYmFyIQ==\"," +
            "  \"ico\" : \"Rm9vYmFyIQ==\"" +
            "}";

    @Test
    public void userJsonTest() {
        try {
            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            User user = mapper.readValue(userJson, User.class);
            System.out.println(user.getName().getFirst());
            System.out.println(mapper.writeValueAsString(user));

            JsonNode rootNode = mapper.readTree(userJson);
            System.out.println(rootNode.path("name").path("last").textValue());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
