package com.magnate.software.common.utils.test.json;

import com.magnate.software.common.utils.HttpClientUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * token测试试类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/10/17 14:40
 */
public class TokenControllerTest {

    private static final String pathUrl = "http://tzh.zhisland.com/bms-api-app";

    private static final String wwwUrl = "http://www.zhisland.com/bms-api-app/";

    private static final String testUrl = "http://192.168.2.93:8090/bms-api-app";

    private static final String serverUrl = "http://192.168.2.69:8090/bms-api-app";

    private static final String localUrl = "http://localhost:8090/bms-api-app";

    @Test
    public void getUserToken() {
        try {
            HttpClient httpClient = HttpClientUtils.getCustomerHttpClient();
            HttpGet httpGet = new HttpGet(serverUrl + "/token/2004819"); // fb51b8f4c99d497b9c6320cd659b5b91
            httpGet.setHeader("appKey", "client-uc");
            HttpResponse httpResponse = httpClient.execute(httpGet);
            System.out.println("HTTP Status: " + httpResponse.getStatusLine().getStatusCode());
            String result = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
            System.out.println("HTTP ReasonPhrase: " + result);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

}
