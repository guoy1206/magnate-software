package com.magnate.software.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 * User: wangyanginfo
 * Date: 12-9-5
 * Time: 上午11:04
 */
public class StringUtils extends org.apache.commons.lang.StringUtils {

    /**
     * 首字母转大写
     *
     * @param str 转换的字符串
     * @return 转换后字符串
     */
    public static String upperCaseFirstChar(String str) {
        if (StringUtils.isBlank(str)) {
            throw new NullPointerException("String param is null");
        }
        String first = (str.substring(0, 1)).toUpperCase();
        String other = str.substring(1);
        return first + other;
    }

    /**
     * 判断一个字符串是否转换为非负整数
     *
     * @param str 字符串
     * @return 真假
     */
    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

    /**
     * 根据表名按规则生成对应的类名称
     * 如果有前缀，则从前缀后开始生成类名
     * 如果有后缀，则从后缀前开始生成类名
     *
     * @param tableName 表名称
     * @param prefix    前缀
     * @param suffix    后缀
     * @param isCase    是否转换小写
     * @return
     */
    public final static String getClassName(String tableName, String prefix, String suffix, boolean isCase) {
        // 断判是否有前缘需要处理
        if (org.apache.commons.lang.StringUtils.isNotBlank(prefix)) {
            int pIndex = tableName.indexOf(prefix);
            if (pIndex > -1) {
                tableName = tableName.substring(prefix.length(), tableName.length());
            }
        }
        // 断判是否有后缀需要处理
        if (org.apache.commons.lang.StringUtils.isNotBlank(suffix)) {
            int sIndex = tableName.indexOf(suffix);
            if (sIndex > -1) {
                tableName = tableName.substring(0, tableName.length() - suffix.length());
            }
        }
        // 处理表名转换为类名，去掉下划线，下划线后首字母大写
        tableName = porssClassName(tableName, isCase);
        // 首字母
        String upperChar = tableName.substring(0, 1);
        // 首字母转大写加上首位后的字符
        tableName = org.apache.commons.lang.StringUtils.upperCase(upperChar) + tableName.substring(1, tableName.length());

        return tableName;
    }

    /**
     * 根据表名按规则生成对应的类名称
     *
     * @param tableName 表名称
     * @param isCase    是否转换小写
     * @return
     */
    public final static String getClassName(String tableName, boolean isCase) throws Exception {
        return getClassName(tableName, null, null, isCase);
    }

    public final static String porssClassName(String tableName, boolean isCase) {
        if (isCase) {
            tableName = org.apache.commons.lang.StringUtils.lowerCase(tableName);
        }

        int index = tableName.indexOf("_");
        if (index > 0) {
            StringBuilder buffer = new StringBuilder(tableName.length());
            buffer.append(tableName.substring(0, index));
            String upperStr = tableName.substring(index + 1, index + 2);
            buffer.append(org.apache.commons.lang.StringUtils.upperCase(upperStr));
            buffer.append(tableName.substring(index + 2, tableName.length()));
            tableName = buffer.toString();
            index = tableName.indexOf("_");
            if (index > 0) {
                tableName = porssClassName(tableName, false);
            }
        }
        return tableName;
    }

    /**
     * 获取首字母大写的名称
     *
     * @param name 名称
     * @return
     */
    public final static String getFieldName(String name) {
        StringBuilder buffer = new StringBuilder(name.length());
        buffer.append(org.apache.commons.lang.StringUtils.upperCase(name.substring(0, 1)));
        buffer.append(name.substring(1, name.length()));
        return name;
    }

    /**
     * 获取以分割符分割的转换后字段名称（各分割后的首字线大写）
     *
     * @param name  名称
     * @param split 分割符
     * @return
     */
    public final static String getFieldName(String name, String split) {
        String[] names = name.split(split);
        StringBuilder buffer = new StringBuilder(name.length());
        for (String str : names) {
            buffer.append(getFieldName(str));
        }
        return name;
    }

    /**
     * 字符名称是否需要转小写
     *
     * @param name   名称
     * @param isCase 是否转小写
     * @return
     */
    public final static String getFieldName(String name, boolean isCase) {
        if (isCase) {
            name = org.apache.commons.lang.StringUtils.lowerCase(name);
        }
        return getFieldName(name);
    }

    /**
     * 字符名称是否需要转小写，并以分割符进行分割转换
     *
     * @param name   名称
     * @param isCase 是否转小写
     * @param split  分割符
     * @return
     */
    public final static String getFieldName(String name, boolean isCase, String split) {
        if (isCase) {
            name = org.apache.commons.lang.StringUtils.lowerCase(name);
        }
        return getFieldName(name, split);
    }

    /**
     * 获取UUID字符串，去除－符号
     *
     * @return
     */
    public final static String randomUUID() {
        String uuid = UUID.randomUUID().toString();
        // 去掉"-"符号
        return uuid.toString().replaceAll("-", "");
    }

    /**
     * 获取定长随机字符串
     *
     * @param length 长度
     * @return
     */
    public static String getRandomString(int length) {
        StringBuilder str = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            String rand = getRandomChar();
            str.append(rand);
        }
        return str.toString();
    }

    /**
     * 获取随机字符或数字
     *
     * @return
     */
    public static String getRandomChar() {
        int index = (int) Math.round(Math.random() * 1);
        String randChar;
        switch (index) {
            case 0://大写字符
                randChar = String.valueOf((char) Math.round(Math.random() * 25 + 65));
                break;
            //case 1://小写字符
            //randChar = String.valueOf((char) Math.round(Math.random() * 25 + 97));
            // break;
            default://数字
                randChar = String.valueOf(Math.round(Math.random() * 9));
                break;
        }
        return randChar;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            //System.out.println(StringUtils.getRandomString(6));
        }

        List<String> list = new ArrayList<String>();
        list.add("AX78XS");
        list.add("AX78XW");

        System.out.println(list.contains("AX78XY"));
    }

}
