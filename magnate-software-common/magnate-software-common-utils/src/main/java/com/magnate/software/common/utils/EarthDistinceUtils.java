package com.magnate.software.common.utils;

/**
 * @author richard.xu
 * 地球表面两点间的距离
 */
public class EarthDistinceUtils {

    private static final double R = 6371229;              //地球的半径
     
	public static double getDistance(double lng1, double lat1, double lng2, double lat2) {
		double x, y, distance;
		x = (lng2 - lng1) * Math.PI * R * Math.cos(((lat1 + lat2) / 2) * Math.PI / 180) / 180;
		y = (lat2 - lat1) * Math.PI * R / 180;
		distance = Math.hypot(x, y);
		return distance;
	}
	
	public static int getIntDistance(double lng1, double lat1, double lng2, double lat2) {
		return new Double(getDistance(lng1, lat1, lng2, lat2)).intValue();
	}
}
