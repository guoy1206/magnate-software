package com.magnate.software.common.utils;

import java.util.Random;

/**
 * 数字工具类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/10/20 10:27
 */
public class NumberUtils {

    /**
     * 生成随机数
     *
     * @param size 位数
     * @return
     */
    public static String randomNumber(Integer size) {
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Random rand = new Random();
        for (int i = 10; i > 1; i--) {
            int index = rand.nextInt(i);
            int tmp = array[index];
            array[index] = array[i - 1];
            array[i - 1] = tmp;
        }
        StringBuilder result = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            result.append(array[i]);
        }
        return result.toString();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(NumberUtils.randomNumber(4));
        }

    }

}
