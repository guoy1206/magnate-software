package com.magnate.software.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: wangyanginfo
 * Date: 12-9-13
 * Time: 下午8:21
 * To change this template use File | Settings | File Templates.
 */
public class DateUtils {

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static final String YYYY_MM_DD_24H_MM_SS = YYYY_MM_DD + " HH:mm:ss";

    public static final String YYYY_MM_DD_12H_MM_SS = YYYY_MM_DD + " hh:mm:ss";

    public static final long DAY_TIME_HOUR = 24;

    public static final long DAY_TIME_MINUTE = DAY_TIME_HOUR * 60;

    public static final long DAY_TIME_SECONDS = DAY_TIME_MINUTE * 60;

    public static final long DAY_TIME_MILLISECONDS = DAY_TIME_SECONDS * 1000;

    /**
     * 将日期字符串转换日期
     *
     * @param day 日期字符串
     * @return
     */
    public static Date toDate(String day) throws Exception {
        DateFormat df = new SimpleDateFormat(YYYY_MM_DD);
        return df.parse(day);
    }

    /**
     * 将当前日期转换为字符串
     *
     * @param format 格式化字符串
     * @return
     */
    public static String toDayToString(String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(new Date());
    }

    /**
     * 将指定日期转换为字符串
     *
     * @param date   日期
     * @param format 格式化字符串
     * @return
     */
    public static String toDateToString(Date date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    /**
     * 格式化输出日期
     *
     * @param time 需要格式化字符串
     * @return
     */
    public static String formatTimeString(String time) {
        if (time == "") {
            return "";
        }
        DateFormat df = new SimpleDateFormat(YYYY_MM_DD);
        return df.format(time);
    }

    /**
     * 获取昨天的字符串表示
     *
     * @return
     */
    public static String yesterDayToString() {
        DateFormat dateFormat = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
        return dateFormat.format(yesterDay());
    }

    /**
     * 获取明天的字符串表示
     *
     * @return
     */
    public static String lastDayToString() {
        DateFormat dateFormat = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
        return dateFormat.format(lastDay());
    }

    /**
     * 当前日期
     *
     * @return
     */
    public static String currentDayToString() {
        DateFormat dateFormat = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
        return dateFormat.format(new Date());
    }


    /**
     * string 转换 date
     *
     * @param time
     * @return
     * @throws ParseException
     */
    public static Date stringTimeToDate(String time) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(DateUtils.YYYY_MM_DD_24H_MM_SS);
        return dateFormat.parse(time);
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    public static String getCurrentTime(Date date) {
        DateFormat dateFormat = new SimpleDateFormat(DateUtils.YYYY_MM_DD_24H_MM_SS);
        return dateFormat.format(date);
    }

    /**
     * 获取昨天日期
     *
     * @return
     */
    public static Date yesterDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.getTime();
    }

    /**
     * 获取明天日期
     *
     * @return
     */
    public static Date lastDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    /**
     * 获取上周，周一、周天的字符串数组
     * 第一个是周一的日期字符串
     * 第二个是周天的日期字符串
     *
     * @return
     */
    public static String[] lastWeekToString() {
        DateFormat dateFormat = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
        Date[] dates = lastWeek();
        String[] datesString = new String[2];
        datesString[0] = dateFormat.format(dates[0]);
        datesString[1] = dateFormat.format(dates[1]);
        return datesString;
    }

    /**
     * 获取上周，周一、周天的日期数组
     * 第一个是周一的日期
     * 第二个是周天的日期
     *
     * @return
     */
    public static Date dateTimeByMinute(int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, minute);
        return calendar.getTime();
    }

    /**
     * 获取上周，周一、周天的日期数组
     * 第一个是周一的日期
     * 第二个是周天的日期
     *
     * @return
     */
    public static Date[] lastWeek() {
        Date[] dates = new Date[2];
        Calendar calendar = Calendar.getInstance();
        int dayWeek = calendar.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
        calendar.add(Calendar.DAY_OF_MONTH, -(5 + dayWeek));
        dates[0] = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, 6);
        dates[1] = calendar.getTime();
        return dates;
    }

    /**
     * 获取本周，周一、周天的字符串数组
     * 第一个是周一的日期字符串
     * 第二个是周天的日期字符串
     *
     * @return
     */
    public static String[] thisWeekToString() {
        DateFormat dateFormat = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
        Date[] dates = thisWeek();
        String[] datesString = new String[2];
        datesString[0] = dateFormat.format(dates[0]);
        datesString[1] = dateFormat.format(dates[1]);
        return datesString;
    }

    /**
     * 获取本周，周一、周天的日期数组
     * 第一个是周一的日期
     * 第二个是周天的日期
     *
     * @return
     */
    public static Date[] thisWeek() {
        Date[] dates = new Date[2];
        Calendar calendar = Calendar.getInstance();
        int dayWeek = calendar.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
        calendar.add(Calendar.DAY_OF_MONTH, -(dayWeek == 1 ? 6 : dayWeek - 2));
        dates[0] = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, 6);
        dates[1] = calendar.getTime();
        return dates;
    }

    /**
     * 获取本月，月头、月尾的字符串数组
     * 第一个是月头的日期字符串
     * 第二个是月尾的日期字符串
     *
     * @return
     */
    public static String[] thisMonthToString() {
        DateFormat dateFormat = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
        Date[] dates = thisMonth();
        String[] datesString = new String[2];
        datesString[0] = dateFormat.format(dates[0]);
        datesString[1] = dateFormat.format(dates[1]);
        return datesString;
    }

    /**
     * 获取本月，月头、月尾的日期数组
     * 第一个是月头的日期
     * 第二个是月尾的日期
     *
     * @return
     */
    public static Date[] thisMonth() {
        Date[] dates = new Date[2];
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -(calendar.get(Calendar.DATE) - 1));
        dates[0] = calendar.getTime();
        calendar.add(Calendar.MONTH, +1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        dates[1] = calendar.getTime();
        return dates;
    }

    /**
     * 获取下月，月头、月尾的字符串数组
     * 第一个是月头的日期字符串
     * 第二个是月尾的日期字符串
     *
     * @return
     */
    public static String[] lastMonthToString() {
        DateFormat dateFormat = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
        Date[] dates = lastMonth();
        String[] datesString = new String[2];
        datesString[0] = dateFormat.format(dates[0]);
        datesString[1] = dateFormat.format(dates[1]);
        return datesString;
    }

    /**
     * 获取下月，月头、月尾的日期数组
     * 第一个是月头的日期
     * 第二个是月尾的日期
     *
     * @return
     */
    public static Date[] lastMonth() {
        Date[] dates = new Date[2];
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -(calendar.get(Calendar.DATE)));
        dates[1] = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, -(calendar.get(Calendar.DATE) - 1));
        dates[0] = calendar.getTime();
        return dates;
    }

    /**
     * 判断当前日期是否是这个月的最后一天
     *
     * @param thisDay 当前日期
     * @return
     */
    public static boolean isLastDayOfMonth(Date thisDay) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, +1);
        calendar.add(Calendar.DAY_OF_MONTH, -(calendar.get(Calendar.DATE)));
        DateFormat df = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
        String lastDayStr = df.format(calendar.getTime());
        String thisDayStr = df.format(thisDay);
        return lastDayStr.equals(thisDayStr);
    }

    /***
     * 日期加一天
     */
    public static String dayAdd(String day) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM_DD);
        Calendar cl = Calendar.getInstance();
        Date date = sdf.parse(day);
        cl.setTime(date);
        // 时间加一天
        cl.add(Calendar.DAY_OF_YEAR, 1);
        date = cl.getTime();
        return sdf.format(date);
    }

    /***
     * 日期减一天
     */
    public static String daySubtract(String day) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM_DD);
        Calendar cl = Calendar.getInstance();
        Date date = sdf.parse(day);
        cl.setTime(date);
        // 时间减一天
        cl.add(Calendar.DAY_OF_YEAR, -1);
        date = cl.getTime();
        return sdf.format(date);
    }

    /**
     * 计算时间间隔 （毫秒）
     *
     * @param firstDate 前时间
     * @param lastDate  后时间
     * @return
     */
    public static long timeInterval(Date firstDate, Date lastDate) {
        return lastDate.getTime() - firstDate.getTime();
    }

    public static void main(String[] args) {
        System.out.println(toDayToString(YYYY_MM_DD_24H_MM_SS));
    }

}
