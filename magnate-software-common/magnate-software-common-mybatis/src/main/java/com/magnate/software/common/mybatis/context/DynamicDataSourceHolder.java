package com.magnate.software.common.mybatis.context;

/**
 * 动态数据库线程本地变量
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/9 16:59
 */
public class DynamicDataSourceHolder {

    public static final ThreadLocal<String> holder = new ThreadLocal<String>();

    public static void putDataSource(String name) {
        holder.set(name);
    }

    public static String getDataSource() {
        return holder.get();
    }

}
