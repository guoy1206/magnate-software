package com.magnate.software.common.mybatis.interceptor;

import com.magnate.software.common.mybatis.context.PagingContextHolder;
import com.magnate.software.common.mybatis.domain.Page;
import com.magnate.software.common.mybatis.utils.ReflectUtils;
import com.magnate.software.common.mybatis.utils.SQLDialectUtils;
import com.magnate.software.common.mybatis.utils.SqlUtils;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.executor.statement.PreparedStatementHandler;
import org.apache.ibatis.executor.statement.RoutingStatementHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.scripting.defaults.DefaultParameterHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

@Intercepts({@Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})})
public class DialectStatementHandlerInterceptor extends SQLDialectUtils implements Interceptor {
    // 数据库方言
    private static String dialect;
    // 日志对象
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    public Object intercept(Invocation invocation) throws Throwable {
        // 翻页参数
        Page page = PagingContextHolder.getPage();
        if (page != null) {
            RoutingStatementHandler statement = (RoutingStatementHandler) invocation.getTarget();
            PreparedStatementHandler delegate = (PreparedStatementHandler) ReflectUtils.getFieldValue(statement, "delegate");
            MappedStatement mappedStatement = (MappedStatement) ReflectUtils.getFieldValue(delegate, "mappedStatement");

            Connection conn = (Connection) invocation.getArgs()[0];
            dialect = conn.getMetaData().getDatabaseProductName().toLowerCase();
            BoundSql boundSql = statement.getBoundSql();
            String sql = boundSql.getSql();
            logger.debug("[BoundSql]" + sql);

            String sqlCount = SqlUtils.getCountString(sql);
            logger.debug("[BoundSql query row count]" + sqlCount);

            // 分页SQL<select>中parameterType属性对应的实体参数，即Mapper接口中执行分页方法的参数,该参数不得为空
            Object parameterObject = boundSql.getParameterObject();
            BoundSql countBS = new BoundSql(mappedStatement.getConfiguration(), sqlCount, boundSql.getParameterMappings(), parameterObject);

            MetaObject metaParameters = (MetaObject)ReflectUtils.getFieldValue(boundSql, "metaParameters");
            ReflectUtils.setFieldValue(countBS, "metaParameters", metaParameters);

            //通过mappedStatement、参数对象page和BoundSql对象countBoundSql建立一个用于设定参数的ParameterHandler对象
            ParameterHandler parameterHandler = new DefaultParameterHandler(mappedStatement, parameterObject, countBS);
            PreparedStatement preparedStatement = conn.prepareStatement(sqlCount);
            parameterHandler.setParameters(preparedStatement);
            // 设置查询参数
            //setParameters(preparedStatement, mappedStatement, countBS, parameterObject);
            // 执行记录总数查询语句
            ResultSet res = preparedStatement.executeQuery();
            // 获得记录总数
            int totalResult = 0;
            if (res.next()) {
                totalResult = res.getInt(1);
            }
            res.close();
            preparedStatement.close();
            // 将记录总数设置到翻页参数中
            page.setTotalResult(totalResult);
            logger.debug("[row count]" + totalResult);

            sql = SQLDialectUtils.getLimitString(dialect, sql, page.getCurrentResult(), page.getPageSize());

            ReflectUtils.setFieldValue(boundSql, "sql", sql);
        }
        return invocation.proceed();
    }

    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    public void setProperties(Properties properties) {
    }

}
