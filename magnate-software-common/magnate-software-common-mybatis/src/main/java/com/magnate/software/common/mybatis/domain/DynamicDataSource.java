package com.magnate.software.common.mybatis.domain;

import com.magnate.software.common.mybatis.context.DynamicDataSourceHolder;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态数据源
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/9 16:53
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceHolder.getDataSource();
    }

}
