package com.magnate.software.anotation;

import java.lang.annotation.*;

/**
 * 日志声明
 * User: wangyanginfo
 * Date: 12-9-5
 * Time: 上午9:40
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableLogger {

    enum Type {LOG4J, SLF4J}

    Type type() default Type.SLF4J;

}
