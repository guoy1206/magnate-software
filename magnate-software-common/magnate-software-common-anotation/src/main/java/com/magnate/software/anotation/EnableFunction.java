package com.magnate.software.anotation;

import java.lang.annotation.*;

/**
 * 功能声明
 * User: WangYang
 * Date: 12-9-5
 * Time: 上午9:40
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableFunction {

    String value() default "";

}
