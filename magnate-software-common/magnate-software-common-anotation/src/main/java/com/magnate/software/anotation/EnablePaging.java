package com.magnate.software.anotation;


import java.lang.annotation.*;

/**
 * 分页注解
 * User: WY
 * Date: 13-4-30
 * Time: 上午12:41
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnablePaging {
    Class pageClass() default Object.class;
}
