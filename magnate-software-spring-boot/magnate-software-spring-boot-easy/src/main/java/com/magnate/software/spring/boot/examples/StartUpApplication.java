package com.magnate.software.spring.boot.examples;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/9 15:37
 */
@SpringBootApplication
public class StartUpApplication {

    public static void main(String[] args) {

        SpringApplication app = new SpringApplication(StartUpApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);

        //new SpringApplicationBuilder(StartUpApplication.class).bannerMode(Banner.Mode.OFF).run(args);
    }
}
