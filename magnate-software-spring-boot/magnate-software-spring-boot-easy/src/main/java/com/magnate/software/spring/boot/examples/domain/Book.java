package com.magnate.software.spring.boot.examples.domain;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

/**
 * 书籍实体对象
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/10 11:40
 */
@Service("book")
@ConfigurationProperties(prefix = "book")
public class Book {

    private String name;
    private String author;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
