package com.magnate.software.spring.boot.examples.controller;

import com.magnate.software.spring.boot.examples.domain.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 控制层
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/9 15:41
 */
@RestController
public class HelloController {

    private static final Logger logger = LoggerFactory.getLogger(HelloController.class);

    @Resource
    private Book book;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String index() {
        return "Hello world";
    }

    @RequestMapping(value = "/book", method = RequestMethod.GET)
    public String getBook() {
        logger.debug("book author：" + book.getAuthor());
        return "Hello world " + book.getAuthor();
    }

}
