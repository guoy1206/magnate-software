package com.magnate.software.spring.boot.web.handler;

import com.magnate.software.spring.boot.web.domain.cms.Code;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/11/17 15:45
 */
public class CodeTypeHandler implements TypeHandler<Code> {

    private static final Logger logger = LoggerFactory.getLogger(CodeTypeHandler.class);

    @Override
    public void setParameter(PreparedStatement ps, int i, Code parameter, JdbcType jdbcType) throws SQLException {
        try {
            ps.setString(i, "");
        } catch (Exception e) {
            logger.debug("Parameter：UserTo Convert String Error", e);
        }
    }

    @Override
    public Code getResult(ResultSet rs, String columnName) throws SQLException {
        try {
            Code code = convert(rs.getString(columnName));
            return code ;
        } catch (Exception e) {
            logger.debug("ResultSet：String Convert UserTo Error", e);
        }
        return null;
    }

    @Override
    public Code getResult(ResultSet rs, int columnIndex) throws SQLException {
        try {
            Code code = convert(rs.getString(columnIndex));
            return code ;
        } catch (Exception e) {
            logger.debug("ResultSet：String Convert UserTo Error", e);
        }
        return null;
    }

    @Override
    public Code getResult(CallableStatement cs, int columnIndex) throws SQLException {
        try {
            convert(cs.getString(columnIndex));
        } catch (Exception e) {
            logger.debug("ResultSet：String Convert UserTo Error", e);
        }
        return null;
    }

    private Code convert(String result) throws Exception {
        return null;
    }
}
