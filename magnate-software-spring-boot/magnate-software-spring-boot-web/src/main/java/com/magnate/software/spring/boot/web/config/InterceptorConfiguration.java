package com.magnate.software.spring.boot.web.config;


import com.magnate.software.spring.boot.web.interceptor.AuthHandlerInterceptor;
import com.magnate.software.spring.boot.web.interceptor.FunctionHandlerInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 拦截器注册
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/28 17:55
 */
@Service("interceptorConfiguration")
public class InterceptorConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册拦截器
        InterceptorRegistration authInterceptor = registry.addInterceptor(new AuthHandlerInterceptor());
        // 配置拦截的路径
        authInterceptor.addPathPatterns("/**");
        // 配置不拦截的路径
        authInterceptor.excludePathPatterns("/user/**");

        // 权限拦截器
        InterceptorRegistration functionInterceptor = registry.addInterceptor(new FunctionHandlerInterceptor());
        // 配置拦截的路径
        functionInterceptor.addPathPatterns("/**");

        super.addInterceptors(registry);
    }
}
