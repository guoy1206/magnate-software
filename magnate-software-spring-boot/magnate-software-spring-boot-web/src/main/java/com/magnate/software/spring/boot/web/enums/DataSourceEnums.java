package com.magnate.software.spring.boot.web.enums;

/**
 * 数据库类型
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/3/2 13:58
 */
public enum DataSourceEnums {

    CMS("cms"),
    TAGS("tags");

    DataSourceEnums(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static DataSourceEnums get(String name) {
        for (DataSourceEnums item : values()) {
            if (name.equals(item.getName())) {
                return item;
            }
        }
        return null;
    }
}
