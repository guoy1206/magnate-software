package com.magnate.software.spring.boot.web.domain.mongo;

import java.io.Serializable;

/**
 * 操作日志
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/26 13:12
 */
public class OperateLogs implements Serializable {

    public static final String MONGO_KEY = "api_operate_log";

    // 唯一ID
    private String uuid;
    // 当前线程标识
    private String threadId;
    // 来源用户ID
    private String sourceUid;
    // 请求路径
    private String url;
    // 请求头
    private String header;
    // 操作时间
    private String time;
    // 来源IP
    private String sourceIp;
    // 目标IP
    private String localIp;
    // 处理时间
    private Long processTime;
    // 方法
    private String method;
    // 类名称
    private String className;
    // 参数
    private String args;
    // 结果
    private String result;

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public String getSourceUid() {
        return sourceUid;
    }

    public void setSourceUid(String sourceUid) {
        this.sourceUid = sourceUid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getLocalIp() {
        return localIp;
    }

    public void setLocalIp(String localIp) {
        this.localIp = localIp;
    }

    public Long getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Long processTime) {
        this.processTime = processTime;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
