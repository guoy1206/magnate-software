package com.magnate.software.spring.boot.web.service;

import com.magnate.software.spring.boot.web.domain.cms.Code;
import com.magnate.software.spring.boot.web.domain.mongo.OperateLogs;
import com.magnate.software.spring.boot.web.domain.tags.Question;

import java.util.List;

/**
 * 用户服务
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/3/2 14:09
 */
public interface UserService {

    /**
     * 根据问题ID获取问题
     *
     * @param id
     * @return
     */
    Question getQuestion(Long id);

    /**
     * 根据条件获取CODE
     *
     * @param code
     * @return
     */
    List<Code> getCode(Code code);

    /**
     * 根据ID获取日志内容
     *
     * @param id
     * @return
     */
    OperateLogs getLogs(String id);

}
