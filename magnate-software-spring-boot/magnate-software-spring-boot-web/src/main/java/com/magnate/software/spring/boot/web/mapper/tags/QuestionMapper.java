package com.magnate.software.spring.boot.web.mapper.tags;

import com.magnate.software.spring.boot.web.enums.DataSourceEnums;
import com.magnate.software.spring.boot.web.anotation.DataSource;
import com.magnate.software.spring.boot.web.domain.tags.Question;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 问题表数据库操作接口
 *
 * @author: WangYang
 * @version: 1.0
 * @datetime: 2016-12-20 10:56:40
 */
@Repository("questionMapper")
public interface QuestionMapper {

    /**
     * 根据主键查询Question对象
     *
     * @param id
     * @return Question对象
     */
    @DataSource(DataSourceEnums.TAGS)
    Question selectByPrimaryKey(Long id);

    /**
     * 根据查询条件查询符合条件的Question对象
     *
     * @param question
     * @return 符合条件的Question对象List
     */
    @DataSource(DataSourceEnums.TAGS)
    List<Question> select(Question question);

    /**
     * 根据主键删除Question对象
     *
     * @param id
     * @return 影响条件数
     */
    @DataSource(DataSourceEnums.TAGS)
    int deleteByPrimaryKey(Long id);

    /**
     * 根据条件删除符合条件的Question对象
     *
     * @param question
     * @return 影响条件数
     */
    @DataSource(DataSourceEnums.TAGS)
    int delete(Question question);

    /**
     * 插入Question对象
     *
     * @param question
     * @return 影响条件数
     */
    @DataSource(DataSourceEnums.TAGS)
    int insert(Question question);

    /**
     * 更新Question对象
     *
     * @param question
     * @return 影响条件数
     */
    @DataSource(DataSourceEnums.TAGS)
    int update(Question question);

    /**
     * 获取标签对应问题ID
     *
     * @param tagsId 标签ID
     * @return
     * @throws Exception
     */
    @DataSource(DataSourceEnums.TAGS)
    List<Long> selectQuestionIdByTags(Long tagsId);

}
