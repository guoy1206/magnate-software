package com.magnate.software.spring.boot.web.domain.cms;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * 字典
 * <p/>
 * User: wangyang
 * DateTime: 2013-12-16 17:26:35
 */
public class Code extends CodeKey {

    private CodeKey key;

    // 字典名称
    private String codeName;
    // 
    private Long codeSuperior;
    // 描述
    @JSONField(serialize = false)
    private String description;
    // 建立时间
    private Date createTime;
    // 更新时间
    private Date updateTime;
    // 删除标识
    @JSONField(serialize = false)
    private Long delTag;

    public CodeKey getKey() {
        return key;
    }

    public void setKey(CodeKey key) {
        this.key = key;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public Long getCodeSuperior() {
        return codeSuperior;
    }

    public void setCodeSuperior(Long codeSuperior) {
        this.codeSuperior = codeSuperior;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getDelTag() {
        return delTag;
    }

    public void setDelTag(Long delTag) {
        this.delTag = delTag;
    }
}
