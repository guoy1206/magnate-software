package com.magnate.software.spring.boot.web.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;

/**
 * 用户控制器
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/3/1 10:48
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String get() {
        return "user";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String get(@PathVariable("id") String id) {
        return MessageFormat.format("user {0}", id);
    }

}
