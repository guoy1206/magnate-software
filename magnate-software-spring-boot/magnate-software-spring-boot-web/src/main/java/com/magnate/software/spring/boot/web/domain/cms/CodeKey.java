package com.magnate.software.spring.boot.web.domain.cms;


/**
 * User: wangyang
 * DateTime: 2013-12-16 17:26:35
 */
public class CodeKey {

    // 分类ID
    private String classId;
    // 字典ID
    private Long codeId;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public Long getCodeId() {
        return codeId;
    }

    public void setCodeId(Long codeId) {
        this.codeId = codeId;
    }

}
