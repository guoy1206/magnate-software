package com.magnate.software.spring.boot.web.anotation;

import com.magnate.software.spring.boot.web.enums.DataSourceEnums;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据据源自定义注解
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/9 17:00
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DataSource {

    DataSourceEnums value();

}
