package com.magnate.software.spring.boot.web.controller;

import com.magnate.software.spring.boot.web.anotation.EnablePaging;
import com.magnate.software.spring.boot.web.domain.cms.Code;
import com.magnate.software.spring.boot.web.domain.mongo.OperateLogs;
import com.magnate.software.spring.boot.web.domain.tags.Question;
import com.magnate.software.spring.boot.web.page.QueryPage;
import com.magnate.software.spring.boot.web.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 其它控制器
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/3/1 10:48
 */
@RestController
@RequestMapping("/other")
public class OtherController {

    private static final Logger logger = LoggerFactory.getLogger(OtherController.class);

    @Resource
    private UserService userService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String get() {
        return "other";
    }

    @RequestMapping(value = "/question/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Question getQuestion(@PathVariable("id") Long id) {
        return userService.getQuestion(id);
    }

    @EnablePaging
    @RequestMapping(value = "/code", method = RequestMethod.GET)
    @ResponseBody
    public List<Code> getCode(@ModelAttribute QueryPage queryPage) {
        logger.info("翻页参数：" + queryPage);
        return userService.getCode(null);
    }

    @EnablePaging
    @RequestMapping(value = "/logs/{id}", method = RequestMethod.GET)
    public OperateLogs getLogs(@PathVariable("id") String id) {
        return userService.getLogs("b9e7a5cdda6a4f359ed69cee576584ec");
    }

}
