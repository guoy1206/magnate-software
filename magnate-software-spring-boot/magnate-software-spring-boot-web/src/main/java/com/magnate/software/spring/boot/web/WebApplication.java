package com.magnate.software.spring.boot.web;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 启动类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/9 15:37
 */
@SpringBootApplication
@EnableAspectJAutoProxy
public class WebApplication {

    public static void main(String[] args) {

        SpringApplication app = new SpringApplication(WebApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);

        //new SpringApplicationBuilder(StartUpApplication.class).bannerMode(Banner.Mode.OFF).run(args);
    }
}
