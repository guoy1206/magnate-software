package com.magnate.software.spring.boot.web.page;

/**
 * 翻页对象
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/10/10 16:00
 */
public class QueryPage {

    // 限制条件
    private String nextId;
    // 当前页数
    private Integer page;
    // 每页显示记录数
    private Integer count;

    public String getNextId() {
        return nextId;
    }

    public void setNextId(String nextId) {
        String[] temps = nextId.split(";");
        if (temps.length  > 1) {
            this.page = Integer.parseInt(temps[0]);
            this.nextId = temps[1];
        } else {
            this.nextId = temps[0];
            try{
                this.page = Integer.valueOf(nextId) ;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        if(count.intValue() == -1) {
            this.count = Integer.MAX_VALUE ;
        }else {
            this.count = count;
        }
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
