package com.magnate.software.spring.boot.web.service.impl;

import com.magnate.software.spring.boot.web.dao.UserDao;
import com.magnate.software.spring.boot.web.domain.cms.Code;
import com.magnate.software.spring.boot.web.domain.mongo.OperateLogs;
import com.magnate.software.spring.boot.web.domain.tags.Question;
import com.magnate.software.spring.boot.web.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户服务实现
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/3/2 14:10
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    @Override
    public Question getQuestion(Long id) {
        return userDao.getQuestion(id);
    }

    @Override
    public List<Code> getCode(Code code) {
        return userDao.getCode(code);
    }

    @Override
    public OperateLogs getLogs(String id) {
        return userDao.getLogs(id);
    }
}
