package com.magnate.software.spring.boot.web.dao;

import com.magnate.software.spring.boot.web.domain.cms.Code;
import com.magnate.software.spring.boot.web.domain.mongo.OperateLogs;
import com.magnate.software.spring.boot.web.domain.tags.Question;

import java.util.List;

/**
 * 用户实体操作接口
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/3/1 19:14
 */
public interface UserDao {

    /**
     * 根据问题ID获取问题
     *
     * @param id
     * @return
     */
    Question getQuestion(Long id);

    /**
     * 根据条件获取CODE
     *
     * @param code
     * @return
     */
    List<Code> getCode(Code code);

    /**
     * 根据ID获取日志内容
     *
     * @param id
     * @return
     */
    OperateLogs getLogs(String id);
}
