package com.magnate.software.spring.boot.web.dao.impl;

import com.magnate.software.spring.boot.web.dao.UserDao;
import com.magnate.software.spring.boot.web.domain.cms.Code;
import com.magnate.software.spring.boot.web.domain.mongo.OperateLogs;
import com.magnate.software.spring.boot.web.domain.tags.Question;
import com.magnate.software.spring.boot.web.mapper.cms.CodeMapper;
import com.magnate.software.spring.boot.web.mapper.tags.QuestionMapper;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户实体操作实现
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/3/1 19:15
 */
@Service("userDao")
public class UserDaoImpl implements UserDao {

    @Resource
    private QuestionMapper questionMapper;

    @Resource
    private CodeMapper codeMapper;

    @Resource
    private MongoTemplate mongoTemplate;

    @Override
    public Question getQuestion(Long id) {
        return questionMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Code> getCode(Code code) {
        return codeMapper.select(code);
    }

    @Override
    public OperateLogs getLogs(String id) {
        return mongoTemplate.findOne(new Query(Criteria.where("uuid").is(id)), OperateLogs.class, OperateLogs.MONGO_KEY);
    }
}
