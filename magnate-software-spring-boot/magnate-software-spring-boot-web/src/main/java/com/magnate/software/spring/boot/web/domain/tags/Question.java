package com.magnate.software.spring.boot.web.domain.tags;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

/**
* 问题表
*
* @author: WangYang
* @version: 1.0
* @datetime: 2016-12-20 10:56:40
*/
public class Question {

    // 问题ID
    private Long id;
    // 问题
    private String question;
    // 类型
    @JsonIgnore
    private Short questionType;
    // 来源
    @JsonIgnore
    private Short source;
    // 状态
    @JsonIgnore
    private Byte status;
    // 建立用户ID
    @JsonIgnore
    private Long createUid;
    // 建立时间
    @JsonIgnore
    private Date createTime;
    // 更新时间
    @JsonIgnore
    private Date updateTime;
    // 删除标识
    @JsonIgnore
    private Byte delTag;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Short getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Short questionType) {
        this.questionType = questionType;
    }

    public Short getSource() {
        return source;
    }

    public void setSource(Short source) {
        this.source = source;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Long getCreateUid() {
        return createUid;
    }

    public void setCreateUid(Long createUid) {
        this.createUid = createUid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getDelTag() {
        return delTag;
    }

    public void setDelTag(Byte delTag) {
        this.delTag = delTag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
