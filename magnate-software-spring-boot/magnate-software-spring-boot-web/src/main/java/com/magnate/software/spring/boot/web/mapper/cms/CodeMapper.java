package com.magnate.software.spring.boot.web.mapper.cms;

import com.magnate.software.spring.boot.web.anotation.DataSource;
import com.magnate.software.spring.boot.web.domain.cms.Code;
import com.magnate.software.spring.boot.web.domain.cms.CodeKey;
import com.magnate.software.spring.boot.web.enums.DataSourceEnums;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Code
 * <p/>
 * User: wangyang
 * DateTime: 2013-12-16 17:26:24
 */
@Repository("codeMapper")
public interface CodeMapper {

    /**
     * 根据查询条件查询符合条件的Code对象
     *
     * @param code
     * @return 符合条件的Code对象List
     */
    @DataSource(DataSourceEnums.CMS)
    List<Code> select(Code code);

    /**
     *  查询字典树
     * @param code 字典信息
     * @return  字典信息结合
     */
    @DataSource(DataSourceEnums.CMS)
    List<Code> queryCodeTree(Code code);

    /**
     * 根据主键删除Code对象
     *
     * @param key
     * @return 影响条件数
     */
    @DataSource(DataSourceEnums.CMS)
    int deleteByPrimaryKey(CodeKey key);

    /**
     * 根据条件删除符合条件的Code对象
     *
     * @param code
     * @return 影响条件数
     */
    @DataSource(DataSourceEnums.CMS)
    int delete(Code code);

    /**
     * 插入Code对象
     *
     * @param code
     * @return 影响条件数
     */
    @DataSource(DataSourceEnums.CMS)
    int insert(Code code);

    /**
     * 更新Code对象
     *
     * @param code
     * @return 影响条件数
     */
    @DataSource(DataSourceEnums.CMS)
    int update(Code code);

    /**
     * 验证字典唯一性
     * @param param
     * @return 统计结果
     */
    @DataSource(DataSourceEnums.CMS)
    int uniqueCode(Map param);

}
