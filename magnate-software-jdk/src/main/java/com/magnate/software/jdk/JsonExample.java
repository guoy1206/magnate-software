package com.magnate.software.jdk;

import com.alibaba.fastjson.JSON;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/22 15:25
 */
public class JsonExample {

    /*
    [{"phones":["12341111299"],"name":""},{"phones":["12341111099"],"name":""},{"phones":["12341111001"],"name":""},
    {"phones":["12341111002"],"name":""},{"phones":["159-1071-7228"],"name":"宝"},{"phones":["11112222999"],"name":""},
    {"phones":["158-1026-0986"],"name":"啊"},{"phones":["11112222030"],"name":""},
    {"phones":["13651368888"],"name":"郝鸿峰"}]
     */

    public static void main(String[] args) {

        List<PhoneObject> phoneObjectList = new ArrayList<>();

        // 通讯录总个数
        int count = 20000;
        // 手机号是否加分隔符限制
        int phoneLimit = 7;
        // 中文字数限制
        int nameLimit = 3;

        int num  = 0;
        while (num < count) {
            int phoneRandom = (int)(Math.random() * 9 + 1);
            // 生成11位随机数字
            String phone = String.valueOf((long)((Math.random() * 9 + 1) * 10000000000L));
            // 生成小于10的随机数，随机数大于7，则加分隔符 -
            if (phoneRandom > phoneLimit) {
                phone = phone.substring(0,3) + "-" + phone.substring(3,7) + "-" + phone.substring(7,11);
            }
            // 随机生成2个或3个汉字的中文
            int nameRandom = (int)(Math.random() * nameLimit + 1);
            String name = getRandomChar(nameRandom < 2 ? 2 : nameRandom);

            PhoneObject phoneObj = new PhoneObject();
            List<String> phones = new ArrayList<>();
            phones.add(phone);
            phoneObj.setPhones(phones);
            phoneObj.setName(name);
            phoneObjectList.add(phoneObj);
            ++num;
        }

        System.out.println(JSON.toJSON(phoneObjectList));
    }

    private static String getRandomChar(int count) {
        StringBuilder stringBuilder = new StringBuilder();

        int highPos,lowPos;
        Random random = new Random();

        while (count > 0) {
            String str = "";

            highPos = (176 + Math.abs(random.nextInt(39)));
            lowPos = (161 + Math.abs(random.nextInt(93)));

            byte[] b = new byte[2];
            b[0] = (Integer.valueOf(highPos)).byteValue();
            b[1] = (Integer.valueOf(lowPos)).byteValue();

            try {
                str = new String(b, "GBK");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                System.out.println("错误");
            }

            stringBuilder.append(str.charAt(0));

            --count;
        }

        return stringBuilder.toString();
    }

    static class PhoneObject {

        private List<String> phones;

        private String name;

        public PhoneObject() {
        }

        public List<String> getPhones() {
            return phones;
        }

        public void setPhones(List<String> phones) {
            this.phones = phones;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
