package com.magnate.software.jdk.chapter4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

/**
 * 日志工具类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/1/19 15:56
 */
public class LoggerUtil {

    private static Logger logger = LoggerFactory.getLogger(LoggerUtil.class);

    public static Logger get(Class object){
        return logger = LoggerFactory.getLogger(object);
    }

    public static void debug(Supplier<String> msg) {
        if(logger.isDebugEnabled()) {
            logger.debug(msg.get());
        }
    }
}
