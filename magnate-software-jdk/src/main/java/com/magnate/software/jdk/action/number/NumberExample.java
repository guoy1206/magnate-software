package com.magnate.software.jdk.action.number;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 数字流示例
 */
public class NumberExample {

    public static void main(String[] args) {
        /*
        斐波纳契数列是著名的经典编程练习。下面这个数列就是斐波纳契数列的一部分： 0, 1, 1,
        2, 3, 5, 8, 13, 21, 34, 55…数列中开始的两个数字是0和1，后续的每个数字都是前两个数字之和。
        斐波纳契元组序列与此类似，是数列中数字和其后续数字组成的元组构成的序列： (0, 1),
        (1, 1), (1, 2), (2, 3), (3, 5), (5, 8), (8, 13), (13, 21) …
        你的任务是用iterate方法生成斐波纳契元组序列中的前20个元素
        */
        Stream.iterate(0, n -> n + (n + 1))
                .limit(20)
                .forEach(System.out::println);

        Stream.iterate(new int[]{0, 1}, a -> new int[]{a[1], a[0] + a[1]})
                .limit(20)
                .forEach(a -> System.out.println(a[0] + " " + a[1]));

        Stream.iterate(new int[]{0, 1}, a -> new int[]{a[1], a[0] + a[1]})
                .limit(10)
                .map(a -> a[0])
                .forEach(a -> System.out.print(a + " "));

        // 通过随机数构建流
        IntStream.rangeClosed(0, 10)
                .forEach(a -> System.out.print(a + " "));
        System.out.println();

        IntStream.range(0, 10)
                .forEach(a -> System.out.print(a + " "));
        System.out.println();

        // 通过数据构建流
        int[] arrayInt = new int[]{1, 2, 3, 4, 5};
        Arrays.stream(arrayInt)
                .forEach(a -> System.out.print(a + " "));
        System.out.println();

        // 通过值构建流
        Stream.of("Java 8 ", "Lambdas ", "In ", "Action")
                .forEach(a -> System.out.print(a + " "));
        System.out.println();

    }

}
