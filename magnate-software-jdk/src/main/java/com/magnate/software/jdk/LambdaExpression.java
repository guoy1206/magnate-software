package com.magnate.software.jdk;

import javax.swing.text.DateFormatter;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.BinaryOperator;

/**
 * jdk1.8 Lambda Expression demo
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/9/27 10:59
 */
public class LambdaExpression {


    public static void main(String[] args) {
        shape();
    }

    /**
     * 形态
     */
    public static void shape() {
        // 无参数，单行代码
        Runnable runnable = () -> System.out.println();
        // 无参数，多行代码
        Runnable runnableBlock = () -> {
            System.out.println();
        };
        // 单参数，单行代码
        ActionListener actionListener = event -> System.out.println();
        // 多参数，单行代码
        BinaryOperator<Long> binaryOperator = (x, y) -> x + y;
        // 多参数，指定参数类型，单行代码
        BinaryOperator<Long> binaryOperator1 = (Long x, Long y) -> x + y;

        // 引用值，而不是变量
        String param = "param";
        // param = "params";
        // 如果再去改变param的值，下面将会报错，param参数是final的
        ActionListener actionListener1 = event -> System.out.println(param);

        ThreadLocal<DateFormatter> local = ThreadLocal.withInitial(() -> new DateFormatter(new SimpleDateFormat("yyyy-MM-dd")));
        System.out.println(local.get().getFormat().format(new Date()));
    }

}