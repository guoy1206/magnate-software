package com.magnate.software.jdk.domain;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/1/10 15:52
 */
public class User {

    private String name;

    private String city;

    private Integer age;

    public User(String name, String city, Integer age) {
        this.name = name;
        this.city = city;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
