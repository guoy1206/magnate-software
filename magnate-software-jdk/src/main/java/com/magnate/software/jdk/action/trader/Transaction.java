package com.magnate.software.jdk.action.trader;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Data
@RequiredArgsConstructor
public class Transaction {

    @Accessors(chain = true)
    private final Trader trader;
    @Accessors(chain = true)
    private final int year;
    @Accessors(chain = true)
    private final int value;

}
