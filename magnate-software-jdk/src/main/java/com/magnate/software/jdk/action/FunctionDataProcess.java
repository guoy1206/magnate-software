package com.magnate.software.jdk.action;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class FunctionDataProcess {

    static List<Dish> menu = Arrays.asList(
            new Dish("pork", false, 800, Dish.Type.MEAT),
            new Dish("beef", false, 700, Dish.Type.MEAT),
            new Dish("chicken", false, 400, Dish.Type.MEAT),
            new Dish("french fries", true, 530, Dish.Type.OTHER),
            new Dish("rice", true, 350, Dish.Type.OTHER),
            new Dish("season fruit", true, 120, Dish.Type.OTHER),
            new Dish("pizza", true, 550, Dish.Type.OTHER),
            new Dish("prawns", false, 300, Dish.Type.FISH),
            new Dish("salmon", false, 450, Dish.Type.FISH));

    static List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5, 6);

    public static void main(String[] args) {
        List<String> dataList = menu.stream()
                .filter(dish -> dish.getCalories() > 300)
                .map(Dish::getName)
                .limit(3)
                .collect(Collectors.toList());
        log.info("dataList :" + dataList);

        log.info("count {}", integerList.parallelStream().count());
        log.info("min {}", integerList.parallelStream().min(Integer::compareTo));
        log.info("min {}", integerList.parallelStream().reduce(Integer::min));
        log.info("max {}", integerList.parallelStream().max(Integer::compareTo));
        log.info("max {}", integerList.parallelStream().reduce(Integer::max));
        log.info("sum {}", integerList.parallelStream().reduce(0, (a, b) -> a + b));
        log.info("sum {}", integerList.parallelStream().reduce(Integer::sum));
    }
}
