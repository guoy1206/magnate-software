package com.magnate.software.jdk.chapter4;

import org.slf4j.Logger;

/**
 * Java8函数示编程
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/1/19 10:45
 */
public class Chapter4 {

    private static final Logger logger = LoggerUtil.get(Chapter4.class);

    public static void main(String[] args) {
        logger.debug("消息0");
        logger.info("消息1");
        LoggerUtil.debug(() -> "消息2");
    }

}
