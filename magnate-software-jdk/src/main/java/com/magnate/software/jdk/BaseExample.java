package com.magnate.software.jdk;

import org.junit.Test;

/**
 * 基础实例
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/14 10:21
 */
public class BaseExample {

    @Test
    public void integerTest() {
        int i = 2556;
        Integer io = 2556;
        Integer io1 = 2556;
        Integer io2 = new Integer(2556);
        System.out.println(io == i);
        System.out.println(io.equals(i));
        System.out.println(io1 == io);
        System.out.println(io1.equals(io));
        System.out.println(io2 == i);
        System.out.println(io2.equals(i));
    }

}
