package com.magnate.software.jdk.action.trader;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Data
@RequiredArgsConstructor
public class Trader {

    @Accessors(chain = true)
    private final String name;
    @Accessors(chain = true)
    private final String city;

}
