package com.magnate.software.jdk.action.trader;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class TraderExample {

    static Trader raoul = new Trader("Raoul", "Cambridge");
    static Trader mario = new Trader("Mario", "Milan");
    static Trader alan = new Trader("Alan", "Cambridge");
    static Trader brian = new Trader("Brian", "Cambridge");

    static List<Transaction> transactions = Arrays.asList(
            new Transaction(brian, 2011, 300),
            new Transaction(raoul, 2012, 1000),
            new Transaction(raoul, 2011, 400),
            new Transaction(mario, 2012, 710),
            new Transaction(mario, 2012, 700),
            new Transaction(alan, 2012, 950)
    );

//    (1) 找出2011年发生的所有交易，并按交易额排序（从低到高）。
//    (2) 交易员都在哪些不同的城市工作过？
//    (3) 查找所有来自于剑桥的交易员，并按姓名排序。
//    (4) 返回所有交易员的姓名字符串，按字母顺序排序。
//    (5) 有没有交易员是在米兰工作的？
//    (6) 打印生活在剑桥的交易员的所有交易额。
//    (7) 所有交易中，最高的交易额是多少？
//    (8) 找到交易额最小的交易。

    public static void main(String[] args) {
        log.info("(1)找出2011年发生的所有交易，并按交易额排序（从低到高）。");
        List<Transaction> transactions1 = transactions.parallelStream()
                .filter(transaction -> 2011 == transaction.getYear())
                .sorted(Comparator.comparingInt(Transaction::getValue))
                //.sorted(Comparator.comparingInt(Transaction::getValue).reversed())
                .collect(Collectors.toList());
        log.info(transactions1.toString());

        log.info("(2)交易员都在哪些不同的城市工作过？");
        List<String> city1 = transactions.parallelStream()
                .map(transaction -> transaction.getTrader().getCity())
                .distinct()
                .collect(Collectors.toList());
        log.info(city1.toString());

        log.info("(3)查找所有来自于剑桥的交易员，并按姓名排序。");
        List<String> name1 = transactions.parallelStream()
                .filter(transaction -> "Cambridge".equals(transaction.getTrader().getCity()))
                .map(transaction -> transaction.getTrader().getName())
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        log.info(name1.toString());

        log.info("(4)返回所有交易员的姓名字符串，按字母顺序排序。");
        String name2 = transactions.parallelStream()
                .map(transaction -> transaction.getTrader().getName())
                .distinct()
                .sorted()
                //.reduce(" ", (a, b) -> a + b);
                .collect(Collectors.joining(" "));
        log.info(name2.toString());

        log.info("(5)有没有交易员是在米兰工作的？");
        List<String> name3 = transactions.parallelStream()
                .filter(transaction -> "Milan".equals(transaction.getTrader().getCity()))
                .map(transaction -> transaction.getTrader().getName())
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        log.info(name3.toString());

        boolean milan = transactions.parallelStream()
                .anyMatch(transaction -> "Milan".equals(transaction.getTrader().getCity()));
        log.info(String.valueOf(milan));

        log.info("(6) 打印生活在剑桥的交易员的所有交易额。");
        List<Integer> value1 = transactions.parallelStream()
                .filter(transaction -> "Milan".equals(transaction.getTrader().getCity()))
                .map(transaction -> transaction.getValue())
                .collect(Collectors.toList());
        log.info(value1.toString());

        transactions.parallelStream()
                .filter(transaction -> "Milan".equals(transaction.getTrader().getCity()))
                .map(transaction -> transaction.getValue())
                .forEach(System.out::println);

        log.info("(7) 所有交易中，最高的交易额是多少？");
        Optional<Integer> value2 = transactions.parallelStream()
                .max(Comparator.comparingInt(Transaction::getValue))
                .map(Transaction::getValue);
        log.info(value2.toString());

        log.info("(8) 找到交易额最小的交易。");
        Optional<Transaction> value3 = transactions.parallelStream()
                .min(Comparator.comparingInt(Transaction::getValue));
        log.info(value3.toString());

    }

}
