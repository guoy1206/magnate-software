package com.magnate.software.domain.po;

import org.msgpack.annotation.Message;

import java.io.Serializable;

/**
 * 用户
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/9/29 16:38
 */
@Message
public class User implements Serializable {

    private static final long serialVersionUID = -7898194272881238670L;

    // 姓名
    private String name;

    // 电话
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
