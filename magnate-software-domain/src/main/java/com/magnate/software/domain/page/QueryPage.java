package com.magnate.software.domain.page;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/5 15:22
 */
public class QueryPage {

    //每页显示的记录数
    private Integer rows;

    //当前第几页
    private Integer page;

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

}
