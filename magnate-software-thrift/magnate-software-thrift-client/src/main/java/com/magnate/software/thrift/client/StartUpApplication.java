package com.magnate.software.thrift.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 启动类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/9 15:37
 */
@SpringBootApplication
@EnableTransactionManagement
public class StartUpApplication {

    private Logger logger = LoggerFactory.getLogger(StartUpApplication.class);

    public static void main(String[] args) {

        SpringApplication app = new SpringApplication(StartUpApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);

        //new SpringApplicationBuilder(StartUpApplication.class).bannerMode(Banner.Mode.OFF).run(args);
    }

}
