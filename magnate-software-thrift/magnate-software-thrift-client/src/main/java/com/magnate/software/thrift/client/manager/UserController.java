package com.magnate.software.thrift.client.manager;

import com.magnate.software.thrift.api.dto.UserDTO;
import com.magnate.software.thrift.api.service.UserService;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 控制层
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/9 15:41
 */
@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private UserService.Iface userService;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public UserDTO getUserByUid() {
        UserDTO user = null;
        try {
            user = userService.getUserByUid(123);
        } catch (TException e) {
            e.printStackTrace();
        }
        return user;
    }


}
