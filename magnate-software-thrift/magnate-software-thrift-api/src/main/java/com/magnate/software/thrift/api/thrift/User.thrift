namespace java com.magnate.software.thrift.api.dto

enum UserSex {
    men=1,
    women=2,
    other=3
}

struct UserDTO {
    1:i64 uid
    2:string name
    3:i32 age
    4:i32 sex
    5:string city
}