namespace java com.magnate.software.thrift.api.service

include "User.thrift"

service UserService {

    User.UserDTO getUserByUid(1:i64 uid)

    list<User.UserDTO> getUserBySex(1:User.UserSex sex)

}