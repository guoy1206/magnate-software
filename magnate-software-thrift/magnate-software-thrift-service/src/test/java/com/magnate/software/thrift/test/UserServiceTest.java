package com.magnate.software.thrift.test;

import com.magnate.software.thrift.api.dto.UserDTO;
import com.magnate.software.thrift.api.service.UserService;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.THttpClient;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/16 14:17
 */
public class UserServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceTest.class);

    private static final String url = "http://localhost:9001/thrift/api/user";

    @Test
    public void userTest() {

        try {
            THttpClient tHttpClient = new THttpClient(url);
            TProtocol protocol = new TCompactProtocol(tHttpClient);

            UserService.Client client = new UserService.Client(protocol);
            UserDTO user = client.getUserByUid(123);
            logger.info("User Name：" + user.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
