package com.magnate.software.thrift.service.implement;

import com.magnate.software.thrift.api.dto.UserDTO;
import com.magnate.software.thrift.api.dto.UserSex;
import com.magnate.software.thrift.api.service.UserService;
import org.apache.thrift.TException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户业务实现
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/16 11:30
 */
@Service("userService")
public class UserServiceImpl implements UserService.Iface {

    @Override
    public UserDTO getUserByUid(long uid) throws TException {
        UserDTO user = new UserDTO();
        user.setUid(123456);
        user.setAge(20);
        user.setName("thrift");
        user.setCity("北京");
        return user;
    }

    @Override
    public List<UserDTO> getUserBySex(UserSex sex) throws TException {
        List<UserDTO> userList = new ArrayList<>();
        UserDTO user = new UserDTO();
        user.setUid(123456);
        user.setAge(20);
        user.setName("thrift");
        user.setCity("北京");
        userList.add(user);
        return userList;
    }

}
