package com.magnate.software.generator.mybatis.utils;

import org.apache.commons.lang.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: wangyanginfo
 * Date: 12-9-24
 * Time: 下午2:57
 * To change this template use File | Settings | File Templates.
 */
public class TableUtils {

    /**
     * 根据表名按规则生成对应的类名称
     * 如果有前缀，则从前缀后开始生成类名
     * 如果有后缀，则从后缀前开始生成类名
     *
     * @param tableName 表名称
     * @param prefix    前缀
     * @param suffix    后缀
     * @param isCase    是否转换小写
     * @return
     */
    public final static String getClassName(String tableName, String prefix, String suffix, boolean isCase) {
        // 断判是否有前缘需要处理
        if (prefix != null && prefix.trim().length() > 0) {
            int pIndex = tableName.indexOf(prefix);
            if (pIndex > -1) {
                tableName = tableName.substring(prefix.length(), tableName.length());
            }
        }
        // 断判是否有后缀需要处理
        if (suffix != null && suffix.trim().length() > 0) {
            int sIndex = tableName.indexOf(suffix);
            if (sIndex > -1) {
                tableName = tableName.substring(0, tableName.length() - suffix.length());
            }
        }
        // 处理表名转换为类名，去掉下划线，下划线后首字母大写
        tableName = porssClassName(tableName, isCase);
        // 首字母
        String upperChar = tableName.substring(0, 1);
        // 首字母转大写加上首位后的字符
        tableName = StringUtils.upperCase(upperChar) + tableName.substring(1, tableName.length());

        return tableName;
    }

    /**
     * 根据表名按规则生成对应的类名称
     *
     * @param tableName 表名称
     * @param isCase    是否转换小写
     * @return
     */
    public final static String getClassName(String tableName, boolean isCase) throws Exception {
        return getClassName(tableName, null, null, isCase);
    }

    public final static String porssClassName(String tableName, boolean isCase) {
        if (isCase) {
            tableName = StringUtils.lowerCase(tableName);
        }

        int index = tableName.indexOf("_");
        if (index > 0) {
            StringBuilder buffer = new StringBuilder(tableName.length());
            buffer.append(tableName.substring(0, index));
            String upperStr = tableName.substring(index + 1, index + 2);
            buffer.append(StringUtils.upperCase(upperStr));
            buffer.append(tableName.substring(index + 2, tableName.length()));
            tableName = buffer.toString();
            index = tableName.indexOf("_");
            if (index > 0) {
                tableName = porssClassName(tableName, false);
            }
        }
        return tableName;
    }

}
