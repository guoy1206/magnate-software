package com.magnate.software.generator.mybatis.utils;

import com.magnate.software.generator.mybatis.domain.enums.MySqlDataType;
import org.apache.commons.lang.StringUtils;

/**
 * 字段工具
 * User: wangyanginfo
 * Date: 12-9-24
 * Time: 下午1:34
 */
public class FieldUtils {

    /**
     * 获取字段对应JAVA类型
     *
     * @param sqlType 数据库类型
     * @return
     */
    public final static String getFieldNameByMysql(String sqlType) {
        return MySqlDataType.getByMySqlType(sqlType.toUpperCase()).getJavaType();
    }

    /**
     * 获取字段对应JAVA类型
     *
     * @param sqlType 数据库类型
     * @return
     */
    public final static MySqlDataType getFieldTypeByMysql(String sqlType) {
        return MySqlDataType.getByMySqlType(sqlType.toUpperCase());
    }

    /**
     * 获取首字母大写的名称
     *
     * @param name 名称
     * @return
     */
    public final static String getFirstCharToUpper(String name) {
        StringBuilder buffer = new StringBuilder(name.length());
        buffer.append(StringUtils.upperCase(name.substring(0, 1)));
        buffer.append(name.substring(1, name.length()));
        return buffer.toString();
    }

    /**
     * 获取以分割符分割的转换后字段名称（各分割后的首字线大写）
     *
     * @param name  名称
     * @param split 分割符
     * @return
     */
    public final static String getFieldName(String name, String split) {
        String[] names = name.split(split);
        StringBuilder buffer = new StringBuilder(name.length());
        buffer.append(names[0]);
        for (int i = 1; i < names.length; i++) {
            buffer.append(getFirstCharToUpper(names[i]));
        }
        return buffer.toString();
    }

    /**
     * 字符名称是否需要转小写
     *
     * @param name   名称
     * @param isCase 是否转小写
     * @return
     */
    public final static String getFieldName(String name, boolean isCase) {
        if (isCase) {
            name = StringUtils.lowerCase(name);
        }
        return getFirstCharToUpper(name);
    }

    /**
     * 字符名称是否需要转小写，并以分割符进行分割转换
     *
     * @param name   名称
     * @param isCase 是否转小写
     * @param split  分割符
     * @return
     */
    public final static String getFieldName(String name, boolean isCase, String split) {
        if (isCase) {
            name = StringUtils.lowerCase(name);
        }
        return getFieldName(name, split);
    }

}
