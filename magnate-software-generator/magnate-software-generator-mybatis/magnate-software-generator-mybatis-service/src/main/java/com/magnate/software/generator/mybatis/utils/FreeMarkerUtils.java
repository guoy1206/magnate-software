package com.magnate.software.generator.mybatis.utils;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/11 10:15
 */
public class FreeMarkerUtils {

    private static Object lock = new Object();

    private static Configuration instance;

    private FreeMarkerUtils() {
    }

    public static Configuration getConfiguration() {

        if (null != instance) {
            return instance;
        }

        //用同步代码块替代方法同步，提高效率，因为绝大部分调用不必进入同步块
        synchronized (lock) {
            //二次空校验，在有两个及以及并发调用时，避免实例被重复创建
            if (null == instance) {
                //声明一个局部变更，避免因构造函数执行期内，并发调用会返回一个未构造完成的实例引用
            /* 在整个应用的生命周期中，这个工作你应该只做一次。 */
            /* 创建和调整配置。 */
                Configuration configuration = new Configuration(Configuration.VERSION_2_3_23);
                // 编码设置1
                configuration.setDefaultEncoding("UTF-8");
                configuration.setObjectWrapper(new DefaultObjectWrapper(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS));
                instance = configuration;
            }
        }
        return instance;
    }

}
