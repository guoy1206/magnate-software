package com.magnate.software.generator.mybatis.service;

/**
 * 代码自动生成服务
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/12/2 13:23
 */
public interface GeneratorService {

    /**
     * 生成mybatis代码
     *
     * @throws Exception
     */
    void mybatis() throws Exception;

}
