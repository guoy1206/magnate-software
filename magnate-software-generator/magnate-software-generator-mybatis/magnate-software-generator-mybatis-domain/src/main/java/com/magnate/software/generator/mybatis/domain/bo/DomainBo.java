package com.magnate.software.generator.mybatis.domain.bo;

import com.magnate.software.generator.mybatis.domain.vo.FieldVo;

import java.util.List;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/12/3 16:19
 */
public class DomainBo extends FileHeaderBo {

    // 主键
    private List<FieldVo> keys;
    // 表中包含的列
    private List<FieldVo> columns;

    public List<FieldVo> getKeys() {
        return keys;
    }

    public void setKeys(List<FieldVo> keys) {
        this.keys = keys;
    }

    public List<FieldVo> getColumns() {
        return columns;
    }

    public void setColumns(List<FieldVo> columns) {
        this.columns = columns;
    }
}
