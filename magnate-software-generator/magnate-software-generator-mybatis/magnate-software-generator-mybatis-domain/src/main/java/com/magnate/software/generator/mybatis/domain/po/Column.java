package com.magnate.software.generator.mybatis.domain.po;


/**
 * 列结构
 * User: wangyanginfo
 * Date: 12-9-21
 * Time: 上午10:46
 */
public class Column {

    // 列名称
    private String columnName;
    // 数据类型
    private String dataType;
    // 列值
    private String columnKey;
    // 列类型
    private String columnType;
    // 列说明
    private String columnComment;
    // 是否可以为空
    private String isNullable;
    // 是否自增
    private String extra;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getColumnKey() {
        return columnKey;
    }

    public void setColumnKey(String columnKey) {
        this.columnKey = columnKey;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public String getIsNullable() {
        return isNullable;
    }

    public void setIsNullable(String isNullable) {
        this.isNullable = isNullable;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
