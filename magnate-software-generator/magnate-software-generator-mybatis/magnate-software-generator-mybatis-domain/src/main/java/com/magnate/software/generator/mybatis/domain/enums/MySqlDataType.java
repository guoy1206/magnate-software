package com.magnate.software.generator.mybatis.domain.enums;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;

/**
 * MySQL数据库常用数据类型
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/7 14:01
 */
public enum MySqlDataType {

    /* 数字类型 */
    TINYINT("TINYINT", "TINYINT", Byte.class.getSimpleName(), Short.class.getSimpleName(), "", ""),
    SMALLINT("SMALLINT", "SMALLINT", Short.class.getSimpleName(), Integer.class.getSimpleName(), "", ""),
    MEDIUMINT("MEDIUMINT", "MEDIUMINT", Integer.class.getSimpleName(), "", "", ""),
    INT("INT", "INTEGER", Integer.class.getSimpleName(), Long.class.getSimpleName(), "", ""),
    INTEGER("INTEGER", "INTEGER", Integer.class.getSimpleName(), Long.class.getSimpleName(), "", ""),
    BIGINT("BIGINT", "BIGINT", Long.class.getSimpleName(), BigInteger.class.getSimpleName(), "", BigInteger.class.getName()),
    FLOAT("FLOAT", "REAL", Float.class.getSimpleName(), "", "", ""),
    DOUBLE("DOUBLE", "DOUBLE", Double.class.getSimpleName(), "", "", ""),
    DECIMAL("DECIMAL", "DECIMAL", BigDecimal.class.getSimpleName(), "", BigDecimal.class.getName(), ""),
    NUMERIC("NUMERIC", "NUMERIC", BigDecimal.class.getSimpleName(), "", BigDecimal.class.getName(), ""),
    /*日期类型*/
    DATE("DATE", "DATE", Date.class.getSimpleName(), "", Date.class.getName(), ""),
    TIME("TIME", "TIME", Time.class.getSimpleName(), "", Time.class.getName(), ""),
    YEAR("YEAR", "YEAR", Date.class.getSimpleName(), "", Date.class.getName(), ""),
    DATETIME("DATETIME", "TIMESTAMP", java.util.Date.class.getSimpleName(), "", java.util.Date.class.getName(), ""),
    TIMESTAMP("TIMESTAMP", "TIMESTAMP", java.util.Date.class.getSimpleName(), "", java.util.Date.class.getName(), ""),
    /* 字符类型 */
    CHAR("CHAR", "CHAR", String.class.getSimpleName(), "", "", ""),
    VARCHAR("VARCHAR", "VARCHAR", String.class.getSimpleName(), "", "", ""),
    TINYTEXT("TINYTEXT", "VARCHAR", String.class.getSimpleName(), "", "", ""),
    TEXT("TEXT", "VARCHAR", String.class.getSimpleName(), "", "", ""),
    MEDIUMTEXT("MEDIUMTEXT", "VARCHAR", String.class.getSimpleName(), "", "", ""),
    LONGTEXT("LONGTEXT", "VARCHAR", String.class.getSimpleName(), "", "", ""),
    /* 二进制类型 */
    BIT("BIT", "BIT", Boolean.class.getName(), "", "", ""),
    TINYBLOB("TINYBLOB", "TINYBLOB", "byte[]", "", "", ""),
    BLOB("BLOB", "BLOB", "byte[]", "", "", ""),
    BINARY("BINARY", "BINARY", "byte[]", "", "", ""),
    VARBINARY("BINARY", "BINARY", "byte[]", "", "", ""),
    MEDIUMBLOB("MEDIUMBLOB", "MEDIUMBLOB", "byte[]", "", "", ""),
    LONGBLOB("LONGBLOB", "LONGBLOB", "byte[]", "", "", ""),
    /*其它*/
    ENUM("ENUM", "ENUM", String.class.getSimpleName(), "", "", ""),
    SET("SET", "SET", String.class.getSimpleName(), "", "", ""),
    REAL("REAL", "REAL", Float.class.getSimpleName(), "", "", ""),;

    private String mysqlType;
    private String jdbcType;
    private String javaType;
    private String unsignedType;
    private String packageName;
    private String unsignedPackageName;

    MySqlDataType(String mysqlType, String jdbcType, String javaType, String unsignedType, String packageName, String unsignedPackageName) {
        this.mysqlType = mysqlType;
        this.jdbcType = jdbcType;
        this.javaType = javaType;
        this.packageName = packageName;
        this.unsignedType = unsignedType;
        this.unsignedPackageName = unsignedPackageName;
    }

    public String getMysqlType() {
        return mysqlType;
    }

    public void setMysqlType(String mysqlType) {
        this.mysqlType = mysqlType;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(String jdbcType) {
        this.jdbcType = jdbcType;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getUnsignedType() {
        return unsignedType;
    }

    public void setUnsignedType(String unsignedType) {
        this.unsignedType = unsignedType;
    }

    public String getUnsignedPackageName() {
        return unsignedPackageName;
    }

    public void setUnsignedPackageName(String unsignedPackageName) {
        this.unsignedPackageName = unsignedPackageName;
    }

    public static MySqlDataType getByMySqlType(String mysqlType) {
        for (MySqlDataType item : MySqlDataType.values()) {
            if (mysqlType.toUpperCase().equals(item.getMysqlType())) {
                return item;
            }
        }
        return null;
    }
}
