package com.magnate.software.generator.mybatis.domain.vo;

/**
 * 基础值类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/12/9 19:41
 */
public class BaseVo {

    // 类型
    private String type;
    // 名称
    private String name;
    // 列名称
    private String colName;
    // 列类型
    private String colType;
    // 是否自增
    private String extra;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
