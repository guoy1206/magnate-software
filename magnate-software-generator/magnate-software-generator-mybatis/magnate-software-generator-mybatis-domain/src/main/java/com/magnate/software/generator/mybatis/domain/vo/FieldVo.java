package com.magnate.software.generator.mybatis.domain.vo;

/**
 * 对象字段
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/12/3 20:44
 */
public class FieldVo extends BaseVo {

    // 说明
    private String comment;
    // 作用域
    private String scope;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
