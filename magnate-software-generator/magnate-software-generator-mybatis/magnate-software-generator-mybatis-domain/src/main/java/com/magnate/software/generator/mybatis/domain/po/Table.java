package com.magnate.software.generator.mybatis.domain.po;

/**
 *  表结构
 * User: wangyanginfo
 * Date: 12-9-21
 * Time: 上午10:46
 *
 */
public class Table {

    // 表名称
    private String tableName;
    // 注解
    private String tableComment;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }
}
