package com.magnate.software.generator.mybatis.domain.vo;

import java.util.List;

/**
 * 方法
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/12/3 20:59
 */
public class MethodVo extends BaseVo {

    // 说明
    private String comment;
    // 作用域
    private String scope;
    // 参数
    private List<BaseVo> params;
    // 是否抛出异常
    private boolean isException;
    // 异常
    private List<String> exceptions;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public List<BaseVo> getParams() {
        return params;
    }

    public void setParams(List<BaseVo> params) {
        this.params = params;
    }

    public boolean isException() {
        return isException;
    }

    public void setIsException(boolean isException) {
        this.isException = isException;
    }

    public List<String> getExceptions() {
        return exceptions;
    }

    public void setExceptions(List<String> exceptions) {
        this.exceptions = exceptions;
    }
}
