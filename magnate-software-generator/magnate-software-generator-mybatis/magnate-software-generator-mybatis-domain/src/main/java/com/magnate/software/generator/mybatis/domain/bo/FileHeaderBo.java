package com.magnate.software.generator.mybatis.domain.bo;

import java.util.Set;

/**
 * 文件头对象
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/12/3 20:33
 */
public class FileHeaderBo {

    // 包申明
    private String packageName;
    // 类名称
    private String className;
    // 需要引入的包
    private Set<String> keyImports;
    // 需要引入的包
    private Set<String> imports;
    // 类说明
    private String comment;
    // 作者
    private String author;
    // 日期
    private String datetime;
    // 表名称
    private String name;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Set<String> getKeyImports() {
        return keyImports;
    }

    public void setKeyImports(Set<String> keyImports) {
        this.keyImports = keyImports;
    }

    public Set<String> getImports() {
        return imports;
    }

    public void setImports(Set<String> imports) {
        this.imports = imports;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
