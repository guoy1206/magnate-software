package com.magnate.software.generator.mybatis.domain.enums;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/11 15:07
 */
public enum Settings {

    AUTHOR("author"),
    DATABASE_NAME("databaseName"),
    BASE_PATH("basePath"),
    DOMAIN_PATH("domianPath"),
    MAPPER_PATH("mapperPath"),
    MAPPER_XML_PATH("mapperXmlPath"),
    DAO_PATH("daoPath"),
    DAO_IMPL_PATH("daoImplPath"),
    DOMAIN_PACKAGE_NAME("domianPackageName"),
    MAPPER_PACKAGE_NAME("mapperPackageName"),
    DAO_PACKAGE_NAME("daoPackageName"),
    DAO_IMPL_PACKAGE_NAME("daoImplPackageName"),
    TABLE_PREFIX("tablePrefix"),
    TABLE_SUFFIX("tableSuffix"),
    ;

    private String value;

    Settings(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
