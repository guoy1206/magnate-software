package com.magnate.software.generator.mybatis.domain.enums;

/**
 * MySQL数据库列键值说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/7 16:41
 */
public enum MySqlKeyType {

    PRI("PRI"), // 主键
    UNI("UNI"),; // 唯一

    private String value;

    MySqlKeyType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
