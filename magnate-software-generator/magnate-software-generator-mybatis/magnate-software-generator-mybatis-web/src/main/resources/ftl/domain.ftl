package ${domain.packageName};

<#if domain.imports??>
    <#list domain.imports as item>
    import ${item};
    </#list>

</#if>
/**
* ${(domain.comment)!}
*
* @author: ${domain.author}
* @version: 1.0
* @datetime: ${domain.datetime}
*/
public class ${domain.className!} <#if domain.keys?? && domain.keys?size gt 1 >extends ${domain.className!}Key</#if> {

<#if domain.keys??>
    <#if domain.keys?size == 1 >
    // ${domain.keys[0].comment!}
    private ${domain.keys[0].type!} ${domain.keys[0].name!};
    </#if>
</#if>
<#if domain.columns??>
    <#list domain.columns as item>
    // ${item.comment!}
    private ${item.type!} ${item.name!};
    </#list>

    <#list domain.columns as item>
    public ${item.type!} get${item.name! ? cap_first}() {
    return ${item.name!};
    }

    public void set${item.name! ? cap_first}(${item.type!} ${item.name!}) {
    this.${item.name!} = ${item.name!};
    }

    </#list>
</#if>
<#if domain.keys??>
    <#if domain.keys?size == 1 >
    public ${domain.keys[0].type!} get${domain.keys[0].name! ? cap_first}() {
    return ${domain.keys[0].name!};
    }

    public void set${domain.keys[0].name! ? cap_first}(${domain.keys[0].type!} ${domain.keys[0].name!}) {
    this.${domain.keys[0].name!} = ${domain.keys[0].name!};
    }
    </#if>
</#if>
}
