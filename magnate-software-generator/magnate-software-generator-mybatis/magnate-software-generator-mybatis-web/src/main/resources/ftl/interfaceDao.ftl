package ${daoPackageName};

import ${domain.packageName}.${domain.className};
<#assign keyType="" >
<#assign paramsName="" >
<#if domain.keys?size gt 1 >
    <#assign keyType="${domain.className!}Key" >
    <#assign paramsName="key" >
import ${domain.packageName}.${keyType};
</#if>
<#if domain.keys?size == 1 >
    <#if domain.keyImports??>
import ${domain.keyImports[0]};
    </#if>
    <#assign keyType="${domain.keys[0].type}" >
    <#assign paramsName="${domain.keys[0].name}" >
</#if>
import java.util.List;

/**
* ${(domain.comment)!}数据操作接口
*
* @author: ${domain.author}
* @version: 1.0
* @datetime: ${domain.datetime}
*
*/
public interface ${domain.className}Dao {

    /**
    *
    * 根据主键查询${domain.className}对象
    *
    * @param ${paramsName}
    * @return ${domain.className}对象
    */
    ${domain.className} get(${keyType} ${paramsName});

    /**
    *
    * 根据查询条件查询符合条件的${domain.className}对象
    *
    * @param ${domain.className ? uncap_first}
    * @return 符合条件的${domain.className}对象List
    */
    List<${domain.className}> get(${domain.className} ${domain.className ? uncap_first});

    /**
    *
    * 根据主键删除${domain.className}对象
    *
    * @param ${paramsName}
    * @return 影响条件数
    */
    int delete(${keyType} ${paramsName});

    /**
    *
    * 根据条件删除符合条件的${domain.className}对象
    *
    * @param ${domain.className ? uncap_first}
    * @return 影响条件数
    */
    int delete(${domain.className} ${domain.className ? uncap_first});

    /**
    *
    * 插入${domain.className}对象
    *
    * @param ${domain.className ? uncap_first}
    * @return 影响条件数
    */
    int save(${domain.className} ${domain.className ? uncap_first});

    /**
    *
    * 更新${domain.className}对象
    *
    * @param ${domain.className ? uncap_first}
    * @return 影响条件数
    */
    int update(${domain.className} ${domain.className ? uncap_first});

}
