package com.magnate.software.generator.mybatis.test;

import com.magnate.software.generator.mybatis.service.GeneratorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * 类说明
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/1/8 10:02
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/spring-config.xml")
public class GeneratorServiceTest {

    @Resource
    private GeneratorService generatorService;

    @Test
    public void mybatisTest() {
        try {
            generatorService.mybatis();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
