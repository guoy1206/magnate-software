<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${mapperPackageName}.${domain.className}Mapper">

<#assign escapeStr="#" >
<#assign colNames="" >
    <resultMap id="BaseResultMap" type="${domain.packageName}.${domain.className}">
    <#if domain.keys??>
        <#list domain.keys as item>
        <id column="${item.colName}" property="${item.name}" jdbcType="${item.colType}"/>
        <#assign colNames="${colNames + item.colName + ','}" >
        </#list>
    </#if>
    <#if domain.columns??>
        <#list domain.columns as item>
        <result column="${item.colName}" property="${item.name}" jdbcType="${item.colType}"/>
        <#assign colNames="${colNames + item.colName + ','}" >
        </#list>
    </#if>
    </resultMap>
<#assign colNames="${colNames ? substring(0,(colNames ? length) - 1)}">

    <sql id="Base_Column_List">
    ${colNames}
    </sql>

    <sql id="where_sql_keys">
        <where>
        <#list domain.keys as item>
            AND ${item.colName} = ${escapeStr}{${item.name},jdbcType=${item.colType}}
        </#list>
        </where>
    </sql>

    <sql id="where_sql">
        <where>
        <#list domain.keys as item>
            <if test="${item.name} != null<#if item.type == 'String'> and ${item.name} != ''</#if>">
            AND ${item.colName} = ${escapeStr}{${item.name},jdbcType=${item.colType}}
            </if>
        </#list>
        <#list domain.columns as item>
            <if test="${item.name} != null<#if item.type == 'String'> and ${item.name} != ''</#if>">
            AND ${item.colName} = ${escapeStr}{${item.name},jdbcType=${item.colType}}
            </if>
        </#list>
        </where>
    </sql>

<#if domain.keys?size gt 1 >
    <#assign keyType="${domain.packageName}.${domain.className!}Key" >
</#if>
<#if domain.keys?size == 1 >
    <#assign keyType= '${domain.keys[0].type}' >
</#if>
    <select id="selectByPrimaryKey" resultMap="BaseResultMap" parameterType="${keyType}">
        SELECT
        <include refid="Base_Column_List"/>
        FROM ${domain.name}
        <include refid="where_sql_keys"/>
    </select>

    <select id="select" resultMap="BaseResultMap" parameterType="${domain.packageName}.${domain.className}">
        SELECT
        <include refid="Base_Column_List"/>
        FROM ${domain.name}
        <include refid="where_sql"/>
    </select>

    <delete id="deleteByPrimaryKey" parameterType="${keyType}">
        DELETE FROM ${domain.name}
        <include refid="where_sql_keys"/>
    </delete>

    <delete id="delete" parameterType="${domain.packageName}.${domain.className}">
        DELETE FROM ${domain.name}
        <include refid="where_sql"/>
    </delete>

    <insert id="insert" parameterType="${domain.packageName}.${domain.className}">
        INSERT INTO ${domain.name}
        <trim prefix="(" suffix=")" suffixOverrides=",">
        <#if domain.keys??>
            <#list domain.keys as item>
            <#if item.extra?? && item.extra != 'auto_increment'>
            <if test="${item.name} != null<#if item.type == 'String'> and ${item.name} != ''</#if>">
                ${item.colName},
            </if>
            </#if>
        </#list>
        </#if>
        <#if domain.columns??>
            <#list domain.columns as item>
            <#if item.extra?? && item.extra != 'auto_increment'>
            <if test="${item.name} != null<#if item.type == 'String'> and ${item.name} != ''</#if>">
                ${item.colName},
            </if>
            </#if>
            </#list>
        </#if>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
        <#if domain.keys??>
            <#list domain.keys as item>
            <#if item.extra?? && item.extra != 'auto_increment'>
            <if test="${item.name} != null<#if item.type == 'String'> and ${item.name} != ''</#if>">
                ${escapeStr}{${item.name},jdbcType=${item.colType}},
            </if>
            </#if>
            </#list>
        </#if>
        <#if domain.columns??>
            <#list domain.columns as item>
            <#if item.extra?? && item.extra != 'auto_increment'>
            <if test="${item.name} != null<#if item.type == 'String'> and ${item.name} != ''</#if>">
                ${escapeStr}{${item.name},jdbcType=${item.colType}},
            </if>
            </#if>
            </#list>
        </#if>
        </trim>
    </insert>

    <update id="update" parameterType="${domain.packageName}.${domain.className}">
        UPDATE ${domain.name}
        <set>
        <#list domain.columns as item>
            <if test="${item.name} != null<#if item.type == 'String'> and ${item.name} != ''</#if>">
                ${item.colName} = ${escapeStr}{${item.name},jdbcType=${item.colType}},
            </if>
        </#list>
        </set>
        <include refid="where_sql_keys"/>
    </update>
</mapper>