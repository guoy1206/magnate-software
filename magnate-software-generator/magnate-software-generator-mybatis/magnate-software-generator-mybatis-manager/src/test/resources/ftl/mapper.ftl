package ${mapperPackageName};

import ${domain.packageName}.${domain.className};
<#assign keyType="" >
<#assign paramsName="" >
<#if domain.keys?size gt 1 >
    <#assign keyType="${domain.className!}Key" >
    <#assign paramsName="key" >
import ${domain.packageName}.${keyType};
</#if>
<#if domain.keys?size == 1 >
    <#if domain.keyImports??>
import ${domain.keyImports[0]};
    </#if>
    <#assign keyType="${domain.keys[0].type}" >
    <#assign paramsName="${domain.keys[0].name}" >
</#if>
import org.springframework.stereotype.Repository;
import java.util.List;

/**
* ${(domain.comment)!}数据库操作接口
*
* @author: ${domain.author}
* @version: 1.0
* @datetime: ${domain.datetime}
*
*/
@Repository("${domain.className ? uncap_first}Mapper")
public interface ${domain.className}Mapper {

    /**
    *
    * 根据主键查询${domain.className}对象
    *
    * @param ${paramsName}
    * @return ${domain.className}对象
    */
    ${domain.className} selectByPrimaryKey(${keyType} ${paramsName});

    /**
    *
    * 根据查询条件查询符合条件的${domain.className}对象
    *
    * @param ${domain.className ? uncap_first}
    * @return 符合条件的${domain.className}对象List
    */
    List<${domain.className}> select(${domain.className} ${domain.className ? uncap_first});

    /**
    *
    * 根据主键删除${domain.className}对象
    *
    * @param ${paramsName}
    * @return 影响条件数
    */
    int deleteByPrimaryKey(${keyType} ${paramsName});

    /**
    *
    * 根据条件删除符合条件的${domain.className}对象
    *
    * @param ${domain.className ? uncap_first}
    * @return 影响条件数
    */
    int delete(${domain.className} ${domain.className ? uncap_first});

    /**
    *
    * 插入${domain.className}对象
    *
    * @param ${domain.className ? uncap_first}
    * @return 影响条件数
    */
    int insert(${domain.className} ${domain.className ? uncap_first});

    /**
    *
    * 更新${domain.className}对象
    *
    * @param ${domain.className ? uncap_first}
    * @return 影响条件数
    */
    int update(${domain.className} ${domain.className ? uncap_first});

}
