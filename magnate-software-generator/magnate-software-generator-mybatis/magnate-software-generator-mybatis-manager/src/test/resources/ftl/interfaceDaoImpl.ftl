package ${daoImplPackageName};

<#assign keyType="" >
<#assign paramsName="" >
<#if domain.keys?size gt 1 >
    <#assign keyType="${domain.className!}Key" >
    <#assign paramsName="key" >
import ${domain.packageName}.${keyType};
</#if>
<#if domain.keys?size == 1 >
    <#if domain.keyImports??>
import ${domain.keyImports[0]};
    </#if>
    <#assign keyType="${domain.keys[0].type}" >
    <#assign paramsName="${domain.keys[0].name}" >
</#if>
import ${daoPackageName}.${(domain.className)!}Dao;
import ${mapperPackageName}.${(domain.className)!}Mapper;
import ${domain.packageName}.${domain.className};
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;


/**
* ${(domain.comment)!}数据操作实现
*
* @author: ${domain.author}
* @version: 1.0
* @datetime: ${domain.datetime}
*
*/
@Service("${domain.className ? uncap_first}Dao")
public class ${(domain.className)!}DaoImpl implements ${(domain.className)!}Dao {

    @Resource
    private ${(domain.className)!}Mapper ${domain.className ? uncap_first}Mapper;

    @Override
    public ${domain.className} get(${keyType} ${paramsName}) {
        return ${domain.className ? uncap_first}Mapper.selectByPrimaryKey(${paramsName});
    }

    @Override
    public List<${domain.className}> get(${domain.className} ${domain.className ? uncap_first}) {
        return ${domain.className ? uncap_first}Mapper.select(${domain.className ? uncap_first});
    }

    @Override
    public int delete(${keyType} ${paramsName}) {
        return ${domain.className ? uncap_first}Mapper.deleteByPrimaryKey(${paramsName});
    }

    @Override
    public int delete(${domain.className} ${domain.className ? uncap_first}) {
        return ${domain.className ? uncap_first}Mapper.delete(${domain.className ? uncap_first});
    }

    @Override
    public int save(${domain.className} ${domain.className ? uncap_first}) {
        return ${domain.className ? uncap_first}Mapper.insert(${domain.className ? uncap_first});
    }

    @Override
    public int update(${domain.className} ${domain.className ? uncap_first}) {
        return ${domain.className ? uncap_first}Mapper.update(${domain.className ? uncap_first});
    }
}
