package ${domain.packageName};

<#if domain.keyImports??>
    <#list domain.keyImports as item>
    import ${item};
    </#list>

</#if>
/**
* ${(domain.comment)!}复合主键
*
* @author: ${domain.author}
* @version: 1.0
* @datetime: ${domain.datetime}
*
*/
public class ${domain.className!}Key {

<#if domain.keys??>
    <#list domain.keys as item>
    // ${item.comment!}
    private ${item.type!} ${item.name!};
    </#list>

    <#list domain.keys as item>
    public ${item.type!} get${item.name! ? cap_first}() {
        return ${item.name!};
    }

    public void set${item.name! ? cap_first}(${item.type!} ${item.name!}) {
        this.${item.name!} = ${item.name!};
    }

    </#list>
</#if>
}