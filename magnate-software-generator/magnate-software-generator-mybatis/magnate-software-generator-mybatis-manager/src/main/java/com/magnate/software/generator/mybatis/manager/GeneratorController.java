package com.magnate.software.generator.mybatis.manager;

import com.magnate.software.generator.mybatis.service.GeneratorService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 代码自动生成控制器
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/12/2 13:31
 */
@RestController
@RequestMapping(value = "/generator")
public class GeneratorController {

    @Resource
    private GeneratorService generatorService;

    /**
     * 生成代码
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void mybatis() throws Exception {
        generatorService.mybatis();
    }

}
