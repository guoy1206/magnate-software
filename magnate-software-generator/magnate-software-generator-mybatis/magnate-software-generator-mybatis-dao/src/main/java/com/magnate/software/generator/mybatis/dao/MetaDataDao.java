package com.magnate.software.generator.mybatis.dao;

import com.magnate.software.generator.mybatis.domain.po.Column;
import com.magnate.software.generator.mybatis.domain.po.Table;

import java.util.List;

/**
 * 数据库结构查询
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/12/2 13:20
 */
public interface MetaDataDao {

    /**
     * 查询数据库表信息
     *
     * @param database 数据库名称
     * @return
     */
    List<Table> getTables(String database);

    /**
     * 查询表中的列信息
     *
     * @param database  数据库名称
     * @param tableName 表名称
     * @return
     */
    List<Column> getColumns(String database, String tableName);

}
