package com.magnate.software.generator.mybatis.dao.impl;

import com.magnate.software.generator.mybatis.dao.MetaDataDao;
import com.magnate.software.generator.mybatis.domain.po.Column;
import com.magnate.software.generator.mybatis.domain.po.Table;
import com.magnate.software.generator.mybatis.mapper.MetaDataMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 数据库结构查询
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/12/2 13:21
 */
@Service("metaDataDao")
public class MetaDataDaoImpl implements MetaDataDao {

    @Resource
    private MetaDataMapper metaDataMapper;

    @Override
    public List<Table> getTables(String database) {
        return metaDataMapper.selectTable(database);
    }

    @Override
    public List<Column> getColumns(String database, String tableName) {
        return metaDataMapper.selectColumn(database, tableName);
    }

}
