package com.magnate.software.generator.mybatis.mapper;

import com.magnate.software.common.mybatis.anotation.DataSource;
import com.magnate.software.generator.mybatis.domain.po.Column;
import com.magnate.software.generator.mybatis.domain.po.Table;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 数据库表结构查询
 * User: WangYang
 * Date: 12-9-24
 * Time: 下午6:01
 */
@Repository("metaDataMapper")
public interface MetaDataMapper {

    /**
     * 查询数据库表信息
     *
     * @param database 数据库名称
     * @return
     */
    @DataSource("cms")
    @Select("SELECT table_name AS tableName,table_comment AS tableComment FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = #{database}")
    List<Table> selectTable(@Param("database") String database);

    /**
     * 查询表中的列信息
     *
     * @param database  数据库名称
     * @param tableName 表名称
     * @return
     */
    @DataSource("cms")
    @Select("SELECT column_name AS `columnName`,data_type AS dataType,column_key AS columnKey,column_type AS columnType," +
            "column_comment AS columnComment, is_nullable AS isNullable,extra " +
            "FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = #{database} AND table_name = #{tableName}")
    List<Column> selectColumn(@Param("database") String database, @Param("tableName") String tableName);
}