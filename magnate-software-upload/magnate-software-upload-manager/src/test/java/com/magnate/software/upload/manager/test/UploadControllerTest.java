package com.magnate.software.upload.manager.test;

import com.magnate.software.common.utils.HttpClientUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 * 上传控制器
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2015/11/26 17:59
 */
public class UploadControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(UploadControllerTest.class);

    @Test
    public void upload() throws Exception {
        try {
            HttpClient httpClient = HttpClientUtils.getCustomerHttpClient();
            HttpPost httpPost = new HttpPost("http://localhost:8080/upload-web/upload");
            int size = 1 * 1024 * 1024;
            final String fileName = "android-sdk-windows.rar";
            RandomAccessFile raFile = new RandomAccessFile("d:/" + fileName, "r");
            long number = raFile.length() / size + (raFile.length() % size > 0 ? 1 : 0);
            for (int i = 0; i < number; i++) {
                int startPoint = i * size;
                long endPoint = (i + 1) * size > raFile.length() ? raFile.length() : (i + 1) * size;
                long dataLength = (i + 1) * size > raFile.length() ? raFile.length() - startPoint : size;
                final byte[] data = new byte[(int) dataLength];
                raFile.seek(startPoint);//跳过前[块数*固定大小 ]个字节
                raFile.read(data);
                ByteArrayBody byteArrayBody = new ByteArrayBody(data, fileName);

                //FileBody fileBody = new FileBody(new File("e:\\1.png"));
                httpPost.setHeader("Content-Range", "bytes " + startPoint + "-" + endPoint + "/" + (raFile.length() + 1));
                //StringBody comment = new StringBody(filename1);
                MultipartEntity reqEntity = new MultipartEntity();
                reqEntity.addPart("file", byteArrayBody);//file1 为请求后台的File upload;属性
                //reqEntity.addPart("filename1", comment);//filename1为请求后台的普通参数;属性
                httpPost.setEntity(reqEntity);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Header[] headers = httpResponse.getAllHeaders();
                for (Header item : headers) {
                    System.out.println("Headers -->" + item.getName() + " = " + item.getValue());
                }
                System.out.println("HTTP Status: " + httpResponse.getStatusLine().getStatusCode());
                System.out.println("HTTP ReasonPhrase: " + EntityUtils.toString(httpResponse.getEntity(), "utf-8"));
            }
            raFile.close();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Test
    public void get() throws Exception {
        try {
            HttpClient httpClient = HttpClientUtils.getCustomerHttpClient();
            HttpGet httpGet = new HttpGet("http://localhost:8080/upload-web/upload");
            HttpResponse httpResponse = httpClient.execute(httpGet);
            System.out.println("HTTP Status: " + httpResponse.getStatusLine().getStatusCode());
            System.out.println("HTTP ReasonPhrase: " + EntityUtils.toString(httpResponse.getEntity(), "utf-8"));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Test
    public void post() throws Exception {
        try {
            HttpClient httpClient = HttpClientUtils.getCustomerHttpClient();
            HttpPost httpPost = new HttpPost("http://localhost:8080/upload-web/upload/get");
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("file_name", "1.png"));
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8")); //将参数填入POST Entity中
            HttpResponse httpResponse = httpClient.execute(httpPost);
            System.out.println("HTTP Status: " + httpResponse.getStatusLine().getStatusCode());
            System.out.println("HTTP ReasonPhrase: " + EntityUtils.toString(httpResponse.getEntity(), "utf-8"));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

}
