package com.magnate.software.spring.cloud.config.kafka;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * 配置服务
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/21 14:13
 */
@EnableConfigServer
@EnableDiscoveryClient
@SpringBootApplication
public class ConfigServerKafkaApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ConfigServerKafkaApplication.class).web(true).run(args);
    }

}
