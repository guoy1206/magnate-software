package com.magnate.software.spring.cloud.config.client.kafka.manager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/21 15:06
 */
@RefreshScope
@RestController
public class ConfigClientController {

    @Value("${from}")
    private String from;

    @RequestMapping(value = "/from", method = RequestMethod.GET)
    public String getFrom() {
        return from;
    }

}
