package com.magnate.software.spring.cloud.config.client.kafka;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 统一配置客户端
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/21 14:59
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ConfigClientKafkaApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ConfigClientKafkaApplication.class).web(true).run(args);
    }

}
