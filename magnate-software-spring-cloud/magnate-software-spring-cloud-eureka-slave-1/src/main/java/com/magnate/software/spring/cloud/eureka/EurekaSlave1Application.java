package com.magnate.software.spring.cloud.eureka;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 启动类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/9 15:37
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaSlave1Application {

    public static void main(String[] args) {
        new SpringApplicationBuilder(EurekaSlave1Application.class).web(true).run(args);
    }
}
