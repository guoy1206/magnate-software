package com.magnate.software.spring.cloud.server.slave;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 启动类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/9 15:37
 */
@EnableDiscoveryClient
@SpringBootApplication
public class SlaveServerApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(SlaveServerApplication.class).web(true).run(args);
    }

}
