package com.magnate.software.spring.cloud.eureka.slave;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 启动类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/9 15:37
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaSlaveApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(EurekaSlaveApplication.class).web(true).run(args);
    }
}
