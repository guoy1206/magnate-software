package com.magnate.software.spring.cloud.server.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 用户服务
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/20 17:51
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private DiscoveryClient client;

    @RequestMapping(value = "" ,method = RequestMethod.GET)
    public String getUser(@RequestParam("uid") Long uid) {
        ServiceInstance instance = client.getLocalServiceInstance();
        logger.info("/add, host:" + instance.getHost() + ", service_id:" + instance.getServiceId() + ", result:" + uid);
        return "USER-SERVER：" + String.valueOf(uid);
    }

}
