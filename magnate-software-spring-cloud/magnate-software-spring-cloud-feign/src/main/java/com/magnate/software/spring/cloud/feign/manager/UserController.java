package com.magnate.software.spring.cloud.feign.manager;

import com.magnate.software.spring.cloud.feign.service.UserServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 用户服务
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/20 17:51
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private UserServiceClient userServiceClient;

    @RequestMapping(value = "/{uid}" ,method = RequestMethod.GET)
    public String getUser(@PathVariable("uid") Long uid) {
        return userServiceClient.getUser(uid);
    }

}
