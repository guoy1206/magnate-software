package com.magnate.software.spring.cloud.feign.service;

import com.magnate.software.spring.cloud.feign.service.impl.UserServiceHystrixClient;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/21 10:12
 */
@FeignClient(value = "user-service", fallback = UserServiceHystrixClient.class)
@Service("userServiceClient")
public interface UserServiceClient {

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    String getUser(@RequestParam("uid") Long uid);

}
