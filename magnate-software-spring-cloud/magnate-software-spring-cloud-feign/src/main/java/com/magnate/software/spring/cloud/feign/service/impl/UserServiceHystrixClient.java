package com.magnate.software.spring.cloud.feign.service.impl;

import com.magnate.software.spring.cloud.feign.service.UserServiceClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/21 11:00
 */
@Service("userServiceHystrixClient")
public class UserServiceHystrixClient implements UserServiceClient {

    @Override
    public String getUser(@RequestParam("uid") Long uid) {
        return "error";
    }
}
