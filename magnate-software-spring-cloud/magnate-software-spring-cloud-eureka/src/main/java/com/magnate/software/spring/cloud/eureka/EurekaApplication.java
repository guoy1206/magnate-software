package com.magnate.software.spring.cloud.eureka;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 启动类
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2016/12/9 15:37
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(EurekaApplication.class);
        app.setWebEnvironment(true);
        app.setBannerMode(Banner.Mode.CONSOLE);
        app.run(args);
    }
}
