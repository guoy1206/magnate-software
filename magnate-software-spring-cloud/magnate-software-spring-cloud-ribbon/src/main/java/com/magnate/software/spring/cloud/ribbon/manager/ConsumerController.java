package com.magnate.software.spring.cloud.ribbon.manager;

import com.magnate.software.spring.cloud.ribbon.service.UserServiceImpl;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * Class Desc
 *
 * @author WangYang
 * @version 1.0
 * @datetime 2017/2/20 18:35
 */
@RestController
@RequestMapping("/user")
public class ConsumerController {

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private UserServiceImpl userService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getUser() {
        return restTemplate.getForEntity("http://user-service/user?uid=20", String.class).getBody();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getUserService(@PathVariable("id") Long id) {
        return userService.getUserService(id);
    }

    @RequestMapping(value = "/batch/{id}", method = RequestMethod.GET)
    public String getUser(@PathVariable("id") Long id) {
        return userService.getUser(id);
    }

}
